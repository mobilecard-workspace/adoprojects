package mx.mobilecard.crypto;

import java.security.MessageDigest;





import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.engines.DESedeEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CryptoAddcel {
	private static final Logger logger = LoggerFactory.getLogger(CryptoAddcel.class);

    private static final byte[] IV_8 = {
        (byte) 0xC6, (byte) 0xBA, (byte) 0xDE, (byte) 0xA4, (byte) 0x76, (byte) 0x43, (byte) 0x32, (byte) 0x6B
    };
	private static  BlockCipher engine = new DESedeEngine();

	public static String encrypt(String key, String plainText) {
		String resp = null;
		try {
			byte[] pbtKey = toBytes(key);
//			byte[] pbtKey = FunctionByte.hex2byte(key);
			byte[] ptBytes = plainText.getBytes();
			logger.debug("KEY Length: " + pbtKey.length);
			logger.debug("CAD Length: " + ptBytes.length);
			
			BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CBCBlockCipher(engine));
			CipherParameters param=new ParametersWithIV(new KeyParameter(pbtKey), IV_8);
			cipher.init(true, param);
			byte[] rv = new byte[cipher.getOutputSize(ptBytes.length)];
			int tam = cipher.processBytes(ptBytes, 0, ptBytes.length, rv, 0);
			
			cipher.doFinal(rv, tam);
			resp = new String(Base64.encodeBase64(rv),"UTF-8");
			
			logger.debug("resp Length: " + resp.getBytes().length);
		} catch (Exception ce) {
			ce.printStackTrace();
		}
		return resp;
	}

	public static String decrypt(String key, String plainText) {
		String resp = null;
		try {
			byte[] pbtKey = toBytes(key);
//			byte[] pbtKey = FunctionByte.hex2byte(key);
			byte[] ptBytes = Base64.decodeBase64(plainText.getBytes("UTF-8"));
			
			logger.debug("KEY Length: " + pbtKey.length);
			logger.debug("CAD Length: " + ptBytes.length);
			
			BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CBCBlockCipher(engine));
			CipherParameters param=new ParametersWithIV(new KeyParameter(pbtKey), IV_8);
			cipher.init(false, param);
			byte[] rv = new byte[cipher.getOutputSize(ptBytes.length)];
			int tam = cipher.processBytes(ptBytes, 0, ptBytes.length, rv, 0);
			
			cipher.doFinal(rv, tam);
			resp = new String(rv);
		} catch (Exception ce) {
			ce.printStackTrace();
		}
		return resp;
	}
	
	/**
     * Metodo para convertir un String en un arreglo de 8 bytes
     * @param s String a convertir
     * @return Un arreglo de 8 bytes
     */
    private static byte[] toBytes(String s){
        byte Resultado[] = null;
        try {
            Resultado = new byte[s.length() / 2];
            for (int i = 0; i < Resultado.length; i++){
                Resultado[i] = (byte)Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16);
            }
            return Resultado;
        }catch(Exception e){
            return Resultado;
        }
    }
    
    public static String md5Base64(String cadena){
    	String cadenaMD5=null;
		try {
			cadenaMD5=new String(Base64.encodeBase64(md5(cadena)));
		} catch (Exception e) {
			logger.error("Error al generar MD5 : ",e);
		}
		return cadenaMD5;
	}
    private static byte[] md5(String cadena){
    	//String cadenaMD5=null;
    	byte[] thedigest=null;
		try {
			byte input[] = cadena.getBytes();
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(input);
			thedigest = md.digest();
			for(byte b:thedigest){
				System.out.print(b+" ");
			}
			//cadenaMD5=new String(thedigest);
		} catch (Exception e) {
			logger.error("Error al generar MD5 : ",e);
		}
		return thedigest;
	}
    public static boolean validaMd5(String cadenaGenerada,String cadenaRecibida){
    	String md5Generado=null;
    	String cadenaMD5=null;
    	try {
    		md5Generado=md5Base64(cadenaGenerada);
    		cadenaMD5=new String(Base64.encodeBase64(Base64.decodeBase64(cadenaRecibida.getBytes())));
    		logger.info("md5Generado : "+md5Generado+ " cadenaMD5 : "+cadenaMD5);
		} catch (Exception e) {
			logger.error("Error al generar MD5 : ",e);
		}
		return cadenaMD5.equals(md5Generado);
    }
    
    public static void main(String args[]){
    	String s=null;
    	s=encrypt("DC101AB52CF894CEE52F61731643B94F","HOLA");    	
    	String str = CryptoAddcel.decrypt("D56BCA85BD854B0085E53DF436D461FC", "WKc7AEY/3Y8v+9tFm4nJYifFDbhoUWPf/BbBWlRJuLZMSRMlUqotfpLDfoL/iDQgp3r7Ox7pNQKA3Xs2ttWoDR0uo0Y9Edtk");
    	System.out.println(str);
    	
    	System.out.println(md5("1^1001^gd432jg3^QgaWA8w+TZFM0S1s3MI925CO7E8WVJC1vXyArDrf4uM="));
    	System.out.println(md5Base64("1^1001^gd432jg3^QgaWA8w+TZFM0S1s3MI925CO7E8WVJC1vXyArDrf4uM="));
    }
}
