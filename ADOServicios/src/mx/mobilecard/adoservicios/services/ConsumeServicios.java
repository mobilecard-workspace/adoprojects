package mx.mobilecard.adoservicios.services;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;

import crypto.Crypto;


/**
 *
 * @author victor
 */
public class ConsumeServicios{

    private static Logger log = Logger.getLogger(ConsumeServicios.class);

    public static String consumeServicio(String urlServicio, HashMap parametros ) {
        InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpsURLConnection conn = null;
        URL url = null;
        String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        try {
            url = new URL(urlServicio);
            conn = (HttpsURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                if (parametros != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write(getQuery(parametros));
                    writer.close();
                    os.close();
                    conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
            }
        } catch (Exception e) {
            log.fatal("Error al leer la url " + url, e);
        } finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                    log.fatal("Exception : ", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    log.fatal("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                    log.fatal("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    log.fatal("Exception : ", e);
                }
            }
        }
        return sbf.toString();
    }

    private static String getQuery(HashMap params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator it = params.entrySet().iterator();
        String p = null;
        while (it.hasNext()) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }
            Map.Entry pairs = (Map.Entry) it.next();
            log.info(pairs.getKey() + " = " + pairs.getValue());
            it.remove(); // avoids a ConcurrentModificationException
            result.append(pairs.getKey());
            result.append("=");
            result.append(pairs.getValue());
        }
        return result.toString();
    }
    
    public static String consumeServicioRESTFul(String urlServicio, String json) {
        InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpURLConnection conn = null;
        URL url = null;
        String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        try {
            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("api_key",Crypto.aesEncrypt("5c55e2ca9737b0ff5d71c458b9421fd9","df2efa060e335f97628ca39c9fef5469ab3cb837"));
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("api_pattern" ,Crypto.aesEncrypt("5c55e2ca9737b0ff5d71c458b9421fd9","wsc@ado.com.mx"));
                conn.setRequestProperty("api_seed",Crypto.aesEncrypt("5c55e2ca9737b0ff5d71c458b9421fd9","juaneloA."));
                log.info("api_key : "+Crypto.aesEncrypt("5c55e2ca9737b0ff5d71c458b9421fd9","df2efa060e335f97628ca39c9fef5469ab3cb837"));
                log.info("api_pattern : "+Crypto.aesEncrypt("5c55e2ca9737b0ff5d71c458b9421fd9","wsc@ado.com.mx"));
                log.info("api_seed : "+ Crypto.aesEncrypt("5c55e2ca9737b0ff5d71c458b9421fd9","juaneloA."));
                if (json != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    log.info("Escribiendo en el servicio : "+json);
                    writer.write(json);
                    writer.flush();
                    writer.close();
                    os.close();
                    //conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
                switch(conn.getResponseCode() ){
	            	case HttpURLConnection.HTTP_CREATED:log.info("HTTP_CREATED : "+conn.getResponseMessage());break;
	            	case HttpURLConnection.HTTP_OK:log.info("HTTP_OK : "+conn.getResponseMessage());break;
	            	case HttpURLConnection.HTTP_BAD_REQUEST:log.info("HTTP_BAD_REQUEST : "+conn.getResponseMessage());break;
	            	case HttpURLConnection.HTTP_UNAUTHORIZED:log.info("HTTP_UNAUTHORIZED : "+conn.getResponseMessage());break;
	            	case HttpURLConnection.HTTP_UNAVAILABLE:log.info("HTTP_UNAVAILABLE : "+conn.getResponseMessage());break;
                	default:log.info("OTHER : "+conn.getResponseCode() +" : "+conn.getResponseMessage());break;
                }     
                log.info(sbf);
            }
        } catch (Exception e) {
            log.fatal("Error al leer la url " + url, e);
        } finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                    log.fatal("Exception : ", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    log.fatal("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                    log.fatal("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    log.fatal("Exception : ", e);
                }
            }
        }
        return sbf.toString();
    }
}