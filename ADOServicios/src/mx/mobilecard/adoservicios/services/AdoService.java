package mx.mobilecard.adoservicios.services;


import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import mx.mobilecard.adoservicios.model.mapper.AdoMapper;
import mx.mobilecard.adoservicios.model.mapper.MobileCardBitacorasMapper;
import mx.mobilecard.adoservicios.model.vo.BitacoraVO;
import mx.mobilecard.adoservicios.model.vo.DatosAdoLogin;
import mx.mobilecard.adoservicios.model.vo.MapeoAdoVO;
import mx.mobilecard.adoservicios.model.vo.RespuestaServicioVO;
import mx.mobilecard.adoservicios.model.vo.TokenVO;
import mx.mobilecard.adoservicios.model.vo.TransactionProcomVO;
import mx.mobilecard.adoservicios.model.vo.request.DatosPago;
import mx.mobilecard.adoservicios.utils.UrlProvider;
import mx.mobilecard.adoservicios.utils.UtilsService;
import mx.mobilecard.crypto.CryptoAddcel;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.mobilecard.adoservicios.model.vo.ProcomVO;

import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import crypto.Crypto;


@Service
public class AdoService {
	private static final Logger logger = LoggerFactory.getLogger(AdoService.class);
	private static final String URL_AUT_PROSA = "http://localhost:8080/ProsaWeb/ProsaAuth?";
	private static final String URL_REV_PROSA = "http://localhost:8080/ProsaWeb/ProsaRev?";
	
	
	private static final String patronCom = "yyyyMMddHHmmss";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);
	private static final String patronEmail = "yyyy-MM-dd hh:mm:ss";
	private static final SimpleDateFormat formatoEmail = new SimpleDateFormat(patronEmail);
	
	
	@Autowired
	private UtilsService utilService;	
	
	@Autowired
	private AdoMapper adoMapper;
	

	@Autowired
	private MobileCardBitacorasMapper bitacorasMapper;
		
	
	public int difFechaMin(String fecha){
		return adoMapper.difFechaMin(fecha);
	}
	
	public MapeoAdoVO existeMapeo(String email){
		return adoMapper.mapeoADO(email);
	}
	public String validaIMEI(String imei){
		return adoMapper.validaIMEI(imei);
	}
	public String validaLogin(String login,String loginMobilecard){
		String res=null;
		res=adoMapper.validaLogin(login);
		if(res!=null&&res.equals(loginMobilecard)){
			res=null;
		}
		return res;
	}
	public String validaEmail(String email,String loginMobilecard){
		String res=null;
		res=adoMapper.validaEmail(email.toLowerCase());
		if(res!=null&&res.equals(loginMobilecard)){
			res=null;
		}
		return res;
	}
	public String validaTDC(String tdc,String loginMobilecard){
		String res=null;
		res=adoMapper.validaTDC(Crypto.aesEncrypt(UrlProvider.parsePass(UtilsService.TDC_PASS), tdc));
		if(res!=null&&res.equals(loginMobilecard)){
			res=null;
		}
		return res;
	}
	public String validaDN(String dn,String loginMobilecard){
		String res=null;
		res=adoMapper.validaTelefono(dn);
		if(res!=null&&res.equals(loginMobilecard)){
			res=null;
		}
		return res;
	}
	public double obtenComision(){
		return adoMapper.obtenComision(Integer.toString(UtilsService.proveedor));
	}
	public int insertaMapeo(MapeoAdoVO mapeo){
		return adoMapper.insertaMapeoADO(mapeo);
	}
	
	public int desligarCuentas(String login){
		return adoMapper.eliminaMapeoADO(login);
	}
	
	public String validaEstatusLogin(String login){
		return adoMapper.validaEstatusLogin(login);
	}
	
	
    public static Object[] validacionLogin(String imei, String login, String password, boolean seguro) throws Exception {
        String urlBase = null;
        String servicio = null;
        String cadenaOriginal = null;
        String cadenaDesencriptada = null;
        String protocol = "http://";
        JSONObject jsonObj = null;
        String jEnc = null;
        HashMap parametros = null;
        Object resultado[] = null;
        logger.info("validacionLogin");
        try {
            jsonObj = new JSONObject();
            jsonObj.put("login", login);
            jsonObj.put("passwordS", Crypto.sha1(password));
            jsonObj.put("password", password);
            jsonObj.put("imei", "ADO" + imei);
            jsonObj.put("tipo", "ADO_app");
            jsonObj.put("software", "1.1.1");
            jsonObj.put("modelo", "web");
            jsonObj.put("key", imei);
            if (seguro) {
                protocol = "https://";
            }
            logger.info(jsonObj.toString());
            jEnc = AddcelCrypto.encryptSensitive(password, jsonObj.toString());

            urlBase = UrlProvider.urlbase;
            servicio = UrlProvider.loginUrl;
            logger.info(new StringBuilder(protocol).append(urlBase).append(servicio).append("?json=").append(jEnc).toString());
            parametros = new HashMap();
            parametros.put("json", jEnc);
            cadenaOriginal = ConsumeServicios.consumeServicio(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), parametros);
            if (cadenaOriginal != null & cadenaOriginal.length() > 0) {
            	logger.info("cadenaOriginal : " + cadenaOriginal);
                cadenaDesencriptada = Crypto.aesDecrypt(UrlProvider.parsePass(password),cadenaOriginal);
                logger.info("cadenaDesencriptada : " + cadenaDesencriptada);
                jsonObj = new JSONObject(cadenaDesencriptada);
                resultado = new Object[2];
                int resultadoInt=jsonObj.getInt("resultado");
                resultado[0] = (resultadoInt==1)?0:(resultadoInt==0)?-1:resultadoInt;
                resultado[1] = (String) jsonObj.get("mensaje");
            } else {
                resultado = null;
            }
        } catch (Exception e) {
        	logger.error("error al validar login : ", e);
            throw e;
        }
        return resultado;
    }
	public String generaToken(String json){				
		TokenVO tokenVO = null;
		try{
			logger.info("Llega : "+json);
			json=json.replaceAll(" ", "+");
			logger.info("Llega : "+Crypto.aesDecrypt(UtilsService.pass,json));
			tokenVO = (TokenVO) utilService.jsonToObject(Crypto.aesDecrypt(UtilsService.pass,json), TokenVO.class);
			if(UtilsService.proveedor!=tokenVO.getIdComercio()||!UtilsService.userToken.equals(tokenVO.getUsuario()) || !UtilsService.passToken.equals(tokenVO.getPassword())){
				json="{\"idError\":2,\"mensajeError\":\"No tiene permisos para consumir este webservice.\"}";
			}else{
				json="{\"token\":\""+Crypto.aesEncrypt(UtilsService.pass,adoMapper.getFechaActual())+"\",\"idError\":0,\"mensajeError\":\"\"}";
			}
		}catch(Exception e){
			json="{\"idError\":1,\"mensajeError\":\"Ocurrio un error al generar el Token.\"}";
			logger.error("Error al generat token", e);
		}
		try{
		json = Crypto.aesEncrypt(UtilsService.passR,json);
		
		}catch(Exception e){
			logger.error("Error al encriptar token : ",e);
		}
		logger.info("Envio : "+json);
		return json;
	}
		
	public String digestEnvioProsa(String varMerchant,String varStore,String varTerm,String varTotal,String varCurrency,String varOrderId){
		return digest(varMerchant+varStore+varTerm+varTotal+varCurrency+varOrderId);		
	}
	private String digest(String text){
		String digest="";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes(),0,text.length());
			byte[] sha1=md.digest();
			digest = convertToHex(sha1);
		} catch (NoSuchAlgorithmException e) {
			logger.info("Error al encriptar - digest", e);
		}
		return digest;
	}
	private String convertToHex(byte[] data) { 
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));    
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    } 
	
	private ProcomVO comercioFin(String user,String referencia,String monto,String idTramite,String email) {    	        
        String varTotal=formatoMontoProsa(monto);      
        //cambiar por datos correctos
        String digest = digestEnvioProsa("7414025", "1234", "001", varTotal, "484", referencia);   
        logger.info("Digest DuttyFree: {}",digest);
        							//(total, currency, addres, orderId, merchant, store, term, digest, urlBack, usuario, idTramite)        
    	ProcomVO procomObj=new ProcomVO(varTotal, "484", "PROSA", referencia, "7414025", "1234", "001", digest, "", user, idTramite,email);
    	
    	logger.info("User DuttyFree: {}",user);
    	logger.info("Referencia DuttyFree: {}",referencia);
    	logger.info("IdTramite DuttyFree: {}",idTramite);
    	
    	return procomObj;
    }
	private String formatoMontoProsa(String monto){
		String varTotal = "000";
		String pesos = null;
        String centavos = null;
		if(monto.contains(".")){
            pesos=monto.substring(0, monto.indexOf("."));
            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
            if(centavos.length()<2){
                centavos = centavos.concat("0");
            }else{
                centavos=centavos.substring(0, 2);
            }            
            varTotal=pesos+centavos;
        }else{
            varTotal=monto.concat("00");
        } 
		logger.info("Monto a cobrar 3dSecure: "+varTotal);
		return varTotal;		
	}
	
	public int insertaBitacora(DatosPago datosPago){
		BitacoraVO bitacora=new BitacoraVO();
		bitacora.setId_usuario(datosPago.getIdUsuario());
		bitacora.setId_proveedor(Integer.toString(UtilsService.proveedor));
		bitacora.setId_producto(UtilsService.producto);
		bitacora.setBit_fecha(new Date(new java.util.Date().getTime()));
		bitacora.setBit_cargo(Double.toString(datosPago.getTotal()));
		bitacora.setBit_concepto("Pago ADO : "+datosPago.getMonto());	
		bitacora.setBit_status("0");
		bitacora.setImei(datosPago.getImei());
		bitacora.setTarjeta_compra(crypto.Crypto.aesEncrypt(UrlProvider.parsePass(UtilsService.TDC_PASS),datosPago.getTarjeta()));
		bitacora.setTipo(datosPago.getTipo());
		bitacora.setSoftware("1.1.1");
		bitacora.setModelo("web");
		bitacora.setWkey(datosPago.getKey());
		bitacora.setDestino(datosPago.getReferenciaAdo());
		if(bitacorasMapper.insertaBitacora(bitacora)>0){
			datosPago.setIdBitacora(Integer.parseInt(bitacora.getId_bitacora()));
		}
		return datosPago.getIdBitacora();
	}
	public int actualizaBitacora(String referencia,String autorizacion,String error,String referenciaADO,String estatus,String ticket){
		BitacoraVO bitacora=new BitacoraVO();
		bitacora.setId_bitacora(referencia);
		bitacora.setBit_no_autorizacion(autorizacion);
		bitacora.setBit_codigo_error(error);
		bitacora.setBit_ticket(ticket);
		bitacora.setBit_status(estatus);
		return bitacorasMapper.actualizaBitacora(bitacora);
	}
	
	public int creaTransaccion(TransactionProcomVO transactionProcomVO){
		return bitacorasMapper.insertaTransaccionProcom(transactionProcomVO);
	}
}