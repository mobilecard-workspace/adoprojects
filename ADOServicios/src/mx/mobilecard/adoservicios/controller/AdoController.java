package mx.mobilecard.adoservicios.controller;


import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.security.KeyPair;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

import javacryption.aes.AesCtr;
import javacryption.jcryption.JCryption;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import crypto.Crypto;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import mx.mobilecard.adoservicios.model.vo.DatosAdoLogin;
import mx.mobilecard.adoservicios.model.vo.MapeoAdoVO;
import mx.mobilecard.adoservicios.model.vo.ProcomVO;
import mx.mobilecard.adoservicios.model.vo.RespuestaServicioVO;
import mx.mobilecard.adoservicios.model.vo.request.DatosPago;
import mx.mobilecard.adoservicios.services.AdoService;
import mx.mobilecard.adoservicios.utils.UtilsService;
import mx.mobilecard.crypto.CryptoAddcel;



/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes({"usuario","datosPago"})
public class AdoController {
	
	private static final Logger logger = LoggerFactory.getLogger(AdoController.class);
	@Autowired
	private AdoService adoPagoService;

	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);				
		return "home";
	}
	

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(@RequestParam("json") String jsonEnc, ModelMap modelo, HttpServletRequest request) {	
		ModelAndView mav = null;
		DatosAdoLogin datosLogin=null;
		Gson gson =null;
		Type collectionType=null;
		String certificado=null;
		StringBuilder certificadoLocal=null;
		double comision=0;
		String loginMC=null;
		DatosPago datosPago=null;
		String loginSugerido=null;
		MapeoAdoVO mapeoAdo=null;
		String loginEstatus=null;
		boolean error=false;
		try{
			try{
				logger.info("Llega : "+jsonEnc);
				datosPago=new DatosPago();
				jsonEnc=jsonEnc.replaceAll(" ", "+");
				jsonEnc=Crypto.aesDecrypt(UtilsService.pass,jsonEnc);
				logger.info("Llega : "+jsonEnc);
				if(!request.isSecure()){
					mav = new ModelAndView("error");
					mav.addObject("idError",200);
					mav.addObject("mensajeError","Error, acceso no permitido HTTP");
					error=true;
	            }else if(jsonEnc!=null&jsonEnc.length()>0){
					gson = new Gson();
					collectionType = new TypeToken<DatosAdoLogin>(){}.getType();
					datosLogin = gson.fromJson(jsonEnc, collectionType);
				}else{
					mav = new ModelAndView("error");
					mav.addObject("idError",8);
					mav.addObject("mensajeError","Ocurrio un error con los datos recibidos");
					error=true;
				}
			}catch(Exception e){
				mav = new ModelAndView("error");
				mav.addObject("idError",8);
				mav.addObject("mensajeError","Ocurrio un error con los datos recibidos");
				logger.error("Ocurrio un error en login : ", e);
			}
			
			if(datosLogin!=null&&datosLogin.validate()){
				//Validar datos del comercio
				if(datosLogin.getIdComercio()!=UtilsService.proveedor){
					mav = new ModelAndView("error");
					mav.addObject("idError",8);
					mav.addObject("mensajeError","Ocurrio un error con los datos recibidos");
					logger.error("Ocurrio un error en login : proveedor incorrecto");
				}else if(!datosLogin.getIdProducto().equals(UtilsService.producto)){
					mav = new ModelAndView("error");
					mav.addObject("idError",8);
					mav.addObject("mensajeError","Ocurrio un error con los datos recibidos");
					logger.error("Ocurrio un error en login : producto incorrecto");
				}else if(datosLogin.getIdConcepto()!=UtilsService.idConcepto){
					mav = new ModelAndView("error");
					mav.addObject("idError",8);
					mav.addObject("mensajeError","Ocurrio un error con los datos recibidos");
					logger.error("Ocurrio un error en login : concepto incorrecto");
				}
				//Valida token
				else if((adoPagoService.difFechaMin(Crypto.aesDecrypt(UtilsService.pass,datosLogin.getToken()))) < 1){
					//Valida certificado
					certificado=datosLogin.getCertificado();
					//Armamos el certificado de nuestro lado
					certificadoLocal=new StringBuilder();
					certificadoLocal.append(datosLogin.getIdComercio());
					certificadoLocal.append("^");
					certificadoLocal.append(datosLogin.getIdConcepto());
					certificadoLocal.append("^");
					certificadoLocal.append(UtilsService.securecode);
					certificadoLocal.append("^");
					certificadoLocal.append(datosLogin.getToken());
					//validamos el certificado
					if(CryptoAddcel.validaMd5(certificadoLocal.toString(),certificado)){
						mapeoAdo=adoPagoService.existeMapeo(datosLogin.getEmail());
						if(mapeoAdo!=null){
							loginMC=mapeoAdo.getLoginMobileCard();
						}
						comision=adoPagoService.obtenComision();
						mav = new ModelAndView("pagoAdo");
						
						datosPago.setComision(comision);
						datosPago.setImei(datosLogin.getImei());
						datosPago.setMonto(datosLogin.getValor());
						datosPago.setEmail(datosLogin.getEmail());
						datosPago.setTotal(datosPago.getComision()+datosPago.getMonto());
						datosPago.setReferenciaAdo(datosLogin.getIdTransaccion());
						datosPago.setIdUsuarioADO(datosLogin.getIdUsuarioMovil());
						datosPago.setKey(datosLogin.getImei());
						mav.addObject("comision",datosPago.getComision());
						mav.addObject("monto",datosPago.getMonto());
						mav.addObject("concepto",datosLogin.getConcepto());
						//LOGIN ASOCIADO
						if(loginMC!=null&&loginMC.length()>0){
							//regresamos la pagina de pago		
							datosPago.setLogin(loginMC);
							mav.addObject("login",loginMC);
							loginEstatus=adoPagoService.validaEstatusLogin(loginMC);
							modelo.addAttribute("datosPago", datosPago);
							logger.info("El estatus del login es :"+loginEstatus);
							if(loginEstatus.equals("1")){
								mav.addObject("idError",0);
								mav.addObject("mensajeError","Exito");
								mav.addObject("pantalla","pago");
							}else if(loginEstatus.equals("98")){
								mav.addObject("idError",98);
								mav.addObject("mensajeError","MODIFIQUE SU CLAVE DE ACCESO POR SEGURIDAD");
								mav.addObject("pantalla","modificarPass");
							}else if(loginEstatus.equals("0")){
								mav= new ModelAndView("error");
								mav.addObject("idError",-1);
								mav.addObject("mensajeError","USUARIO INACTIVO. CONSULTE CON EL ADMINISTRADOR");
								mav.addObject("pantalla","pagelogin");
							}else if(loginEstatus.equals("99")){
								mav.addObject("idError",99);
								mav.addObject("mensajeError","MODIFIQUE SU INFORMACION POR SEGURIDAD");
								mav.addObject("pantalla","pagelogin");
							}
						}else{
							//el imei esta registrado
							loginMC=adoPagoService.validaIMEI("ADO"+datosLogin.getImei());
							if(loginMC!=null&&loginMC.length()>0){
								logger.info("el imei esta registrado");
								datosPago.setLogin(loginMC);
								mav.addObject("idError",0);
								mav.addObject("mensajeError","Exito");
								mav.addObject("login",loginMC);
								mav.addObject("pantalla","pagelogin");
								modelo.addAttribute("datosPago", datosPago);
							}else{
								loginMC=adoPagoService.validaEmail(datosLogin.getEmail(),null);
								if(loginMC!=null&&loginMC.length()>0){
									datosPago.setLogin(loginMC);
									//el email esta registrado
									mav.addObject("idError",0);
									mav.addObject("mensajeError","Exito");
									mav.addObject("login",loginMC);
									mav.addObject("pantalla","pagelogin");
									modelo.addAttribute("datosPago", datosPago);
								}else{
									//el email no esta registrado
									int indice=datosLogin.getEmail().indexOf("@");
									if(indice>0){
										loginSugerido=datosLogin.getEmail().substring(0,indice);
									}
									if(loginSugerido!=null&&loginSugerido.length()>0){
										loginMC=adoPagoService.validaLogin(loginSugerido,null);
									}
									if(loginMC==null||loginMC.length()==0){
										mav.addObject("loginSugerido",loginSugerido);
									}
									mav.addObject("idError",0);
									mav.addObject("mensajeError","Exito");
									mav.addObject("pantalla","welcome");
									mav.addObject("email",datosLogin.getEmail());
									modelo.addAttribute("datosPago", datosPago);
								}
							}
						}
					}else{
						mav = new ModelAndView("error");
						mav.addObject("idError",6);
						mav.addObject("mensajeError","La transacción no es válida");
						logger.error("La Transacción ya no es válida, error al validar certificado");
					}
				}else{
					mav = new ModelAndView("error");
					mav.addObject("idError",5);
					mav.addObject("mensajeError","La Transacción ya no es válida.");
					logger.error("La Transacción ya no es válida.");
				}
			}else if(!error){
				mav = new ModelAndView("error");
				mav.addObject("idError",5);
				mav.addObject("mensajeError","Error al recibir datos de ADO");
				logger.error("Error al recibir datos de ADO, valido los datos, hubo error");
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			mav.addObject("idError",7);
			mav.addObject("mensajeError","Ocurrio un error en la aplicación");
			logger.error("Ocurrio un error en login : ", e);
		}
		return mav;
	}

	@RequestMapping(value = "/getToken", method = RequestMethod.POST)
	public @ResponseBody String getToken(@RequestParam("json") String jsonEnc, HttpServletRequest request) {	
		if(!request.isSecure()){
			return "{\"idError\":200,\"mensajeError\":\"Error, acceso no permitido HTTP\"}";
		}else{
			return adoPagoService.generaToken(jsonEnc);
		}
	}
	
	@RequestMapping(value = "/test")
	public @ResponseBody String test() {		
		StringBuffer html=new StringBuffer();
		
		return "";
	}
	
	@RequestMapping(value = "/crypto", method = RequestMethod.POST)
	public @ResponseBody String crypto(HttpServletRequest request) {		
        PrintWriter out = null;
        String callback = null;
        String element = null;
        String output = null;
        Enumeration en = null;
        JCryption jc = null;
        KeyPair keys = null;
        String key = null;
        String e = null;
        String n = null;
        String md = null;
        en = request.getParameterNames();
        logger.info("***************inicio***************");
        while (en.hasMoreElements()) {
            element = (String) en.nextElement();
            logger.info(element + " : " + request.getParameter(element));
        }
        logger.info("***************fin***************");
        if(!request.isSecure()){
        	return "";
        }
        
        /**
         * Generates a KeyPair for RSA *
         */
        if (request.getParameter("publicKey") != null) {
        	logger.info("llave publica : " + request.getParameter("publicKey"));
            HttpSession session = request.getSession(false);
            if(session!=null){
                session.invalidate();
            }
            session=request.getSession(true);
            session.setAttribute("publicKey", request.getParameter("publicKey"));
        }
        if (request.getParameter("generateKeyPair") != null && request.getParameter("generateKeyPair").equals("true")) {
            jc = new JCryption();
            keys = jc.getKeyPair();
            request.getSession().setAttribute("jCryptionKeys", keys);
            e = jc.getPublicExponent();
            n = jc.getKeyModulus();
            md = String.valueOf(jc.getMaxDigits());
            /**
             * Sends response *
             */
            callback = request.getParameter("callback");
            output = null;
            if (callback != null) {
                //response.setContentType("text/javascript");
                output = callback + "({\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"});";
            } else {
                output = "{\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"}";
            }
            logger.info(output);
            //out = response.getWriter();
            //out.print(output);
            //out.flush();
            return output;
        } /**
         * jCryption handshake *
         */
        else if (request.getParameter("handshake") != null && request.getParameter("handshake").equals("true")) {
            /**
             * Decrypts password using private key *
             */
            jc = new JCryption((KeyPair) request.getSession().getAttribute("jCryptionKeys"));
            key = jc.decrypt(request.getParameter("key"));
            request.getSession().removeAttribute("jCryptionKeys");
            request.getSession().setAttribute("jCryptionKey", key);
            /**
             * Encrypts password using AES *
             */
            String ct = AesCtr.encrypt(key, key, 256);
            /**
             * Sends response *
             */
            callback = request.getParameter("callback");
            if (callback != null) {
                //response.setContentType("text/javascript");
                output = callback + "({\"challenge\":\"" + ct + "\"});";
            } else {
                output = "{\"challenge\":\"" + ct + "\"}";
            }
            logger.info(output);
            //out = response.getWriter();
            //out.print(output);
            //out.flush();
            return output;
        } else if (request.getParameter("generateKeyPair_") != null && request.getParameter("generateKeyPair_").equals("true")) {
            jc = new JCryption();
            keys = jc.getKeyPair();
            request.getSession().setAttribute("jCryptionKeys_", keys);
            e = jc.getPublicExponent();
            n = jc.getKeyModulus();
            md = String.valueOf(jc.getMaxDigits());
            /**
             * Sends response *
             */
            callback = request.getParameter("callback");
            output = null;
            if (callback != null) {
                //response.setContentType("text/javascript");
                output = callback + "({\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"});";
            } else {
                output = "{\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"}";
            }
            logger.info(output);
            //out = response.getWriter();
            //out.print(output);
            //out.flush();
            return output;
        } /**
         * jCryption handshake *
         */
        else if (request.getParameter("handshake_") != null && request.getParameter("handshake_").equals("true")) {
            /**
             * Decrypts password using private key *
             */
            jc = new JCryption((KeyPair) request.getSession().getAttribute("jCryptionKeys_"));
            key = jc.decrypt(request.getParameter("key"));
            request.getSession().removeAttribute("jCryptionKeys_");
            request.getSession().setAttribute("jCryptionKey_", key);
            /**
             * Encrypts password using AES *
             */
            String ct = AesCtr.encrypt(key, key, 256);
            /**
             * Sends response *
             */
            callback = request.getParameter("callback");
            if (callback != null) {
                //response.setContentType("text/javascript");
                output = callback + "({\"challenge\":\"" + ct + "\"});";
            } else {
                output = "{\"challenge\":\"" + ct + "\"}";
            }
            logger.info(output);
            //out = response.getWriter();
            //out.print(output);
            //out.flush();
            return output;
        }
        return "";
    }
	
	/*@RequestMapping(value = "/pago-visa", method = RequestMethod.POST)
	public @ResponseBody String procesaPagoLogin(@RequestParam("json") String jsonEnc) {	
		return megaCablePagoService.procesaPagoLogin(jsonEnc);	
	}	*/
	
	@RequestMapping(value = "/pagina-prosa", method = RequestMethod.POST)
	public String paginaTestProsa(Model model, HttpServletRequest request) {
		logger.info("Dentro del servicio: /pagina-prosa");
		//ProcomVO procomVO=adoPagoService.comercioFin("1231", "1231", "200.00", "3423","vfernando.lira@gmail.com");
		//model.addAttribute("prosa", procomVO);
		return "pagina-prosa";	
	}
	
}