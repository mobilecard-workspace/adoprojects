package mx.mobilecard.adoservicios.controller;

import java.util.HashMap;
import java.util.Map;

import javacryption.aes.AesCtr;

import javax.servlet.http.HttpServletRequest;

import mx.mobilecard.adoservicios.model.vo.request.DatosPago;
import mx.mobilecard.adoservicios.services.AdoService;
import mx.mobilecard.adoservicios.services.ConsumeServicios;
import mx.mobilecard.adoservicios.utils.UrlProvider;
import mx.mobilecard.adoservicios.utils.UtilsService;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.addcel.utils.AddcelCrypto;

import crypto.Crypto;

@Controller
@SessionAttributes({"usuario","datosPago"})
public class EditaController {
	
	private static final Logger logger = LoggerFactory.getLogger(EditaController.class);
	@Autowired
	private AdoService adoPagoService;

	@RequestMapping(value = "/desligarCuenta", method = RequestMethod.POST)
	public @ResponseBody String desligarCuenta(@RequestParam("data") String data, HttpServletRequest request) {	
		String keyResponse=null;
		JSONObject respuesta=null;
		String parametros=null;
		Map <String,String> parametrosMap=null;
		DatosPago datosPago=null;
		Object loginResp[]=null;
		String respServ=null;
		int i=0;
		try{
			logger.info("Entro inicio llega : "+data);
			respuesta=new JSONObject();
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey_");
            if(!request.isSecure()){
            	respuesta.put("idError",200);
            	respuesta.put("errorMensaje", "Error, acceso no permitido HTTP");
            	respServ=respuesta.toString();
            }else if (keyResponse == null || keyResponse.length() == 0) {
            	respuesta.put("idError",4);
            	respuesta.put("errorMensaje", "Error al validar session");
            	respServ=respuesta.toString();
            } else {
            	parametros=AesCtr.decrypt(data, keyResponse, 256);
            	logger.info("Entro inicio llega : "+ parametros);
            	parametrosMap=UtilsService.splitQuery(parametros);
            	datosPago=(DatosPago)request.getSession().getAttribute("datosPago");
            	loginResp=adoPagoService.validacionLogin(datosPago.getImei(),parametrosMap.get("login"),parametrosMap.get("password"),false);
            	if(((Integer)loginResp[0])==0){
            		i=adoPagoService.desligarCuentas(parametrosMap.get("login"));
            		if(i>0){
            			respuesta.put("idError", 1);
                    	respuesta.put("errorMensaje", "SE HAN DELIGADO LAS CUENTAS");
            		}else{
            			respuesta.put("idError", -1);
                    	respuesta.put("errorMensaje", "OCURRIO UN ERROR AL DESLIGAR LAS CUENTAS, INTENTELO NUEVAMENTE");
            		}
            	}else{
            		respuesta.put("idError", loginResp[0]);
                	respuesta.put("errorMensaje", loginResp[1]);
            	}
            	respServ=AesCtr.encrypt(respuesta.toString(), keyResponse, 256);
            }
		}catch(Exception e){
			logger.info("Error en desligarCuenta : ",e);
			respServ="{\"idError\":5,\"errorMensaje\":\"Error en la aplicaci&oacute;n, intente nuevamente\"}";
		}
		logger.info(respServ);
		return respServ;
	}
	
	@RequestMapping(value = "/modificaPass", method = RequestMethod.POST)
	public @ResponseBody String modificaPass(@RequestParam("data") String data, HttpServletRequest request) {	
		JSONObject respuesta=null;
		String parametros=null;
		Map <String,String> parametrosMap=null;
		DatosPago datosPago=null;
		String loginResp[]=null;
		String respServ=null;
		int i=0;
		JSONObject jsonObj=null;
		String key=null;
		String protocol="http://";
		String urlBase=null; 
		String servicio=null;
		String jEnc=null;
		HashMap parametrosMC=null;
		String cadenaOriginal=null;
		String cadenaDesencriptada=null;
		String resultado[]=null;
		try{
			logger.info("Entro inicio llega : "+data);
			respuesta=new JSONObject();
			key = (String) request.getSession().getAttribute("jCryptionKey_");
			if(!request.isSecure()){
            	respuesta.put("idError",200);
            	respuesta.put("errorMensaje", "Error, acceso no permitido HTTP");
            	respServ=respuesta.toString();
            }else if (key == null || key.length() == 0 ) {
            	respuesta.put("idError",4);
            	respuesta.put("errorMensaje", "Error al validar session");
            	respServ=respuesta.toString();
            } else {
                if (request.isSecure()) {
                    protocol = "https://";
                }
                parametros=AesCtr.decrypt(data, key, 256);
            	logger.info("Entro inicio llega : "+ parametros);
            	parametrosMap=UtilsService.splitQuery(parametros);
                jsonObj = new JSONObject();
                jsonObj.put("login", parametrosMap.get("login"));
                jsonObj.put("password", parametrosMap.get("oldPassword"));
                jsonObj.put("newPassword", parametrosMap.get("newPassword"));
                logger.info(jsonObj.toString());
                urlBase = UrlProvider.urlbase;
                servicio = UrlProvider.servicioCambiaPassURL;
                jEnc = AddcelCrypto.encryptSensitive(parametrosMap.get("oldPassword"), jsonObj.toString());
                logger.info(new StringBuilder(protocol).append(urlBase).append(servicio).append("?json=").append(jEnc).toString());
                parametrosMC = new HashMap();
                parametrosMC.put("json", jEnc);
                cadenaOriginal = ConsumeServicios.consumeServicio(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), parametrosMC);
                logger.info("cadenaOriginal : " + cadenaOriginal);
                cadenaDesencriptada = Crypto.aesDecrypt(UrlProvider.parsePass(parametrosMap.get("oldPassword")), cadenaOriginal);
                logger.info("cadenaDesencriptada : " + cadenaDesencriptada);
                jsonObj = new JSONObject(cadenaDesencriptada);
                resultado = new String[2];
                respuesta.put("idError",jsonObj.get("resultado"));
                respuesta.put("errorMensaje",jsonObj.get("mensaje"));
                respServ=AesCtr.encrypt(respuesta.toString(), key, 256);
            }
		}catch(Exception e){
			logger.info("Error en desligarCuenta : ",e);
			respServ="{\"idError\":5,\"errorMensaje\":\"Error en la aplicaci&oacute;n, intente nuevamente\"}";
		}
		logger.info(respServ);
		return respServ;
	}
	
	@RequestMapping(value = "/recuperaPass", method = RequestMethod.POST)
	public @ResponseBody String recuperaPass(@RequestParam("data") String data, HttpServletRequest request) {	
		String keyResponse=null;
		JSONObject respuesta=null;
		String parametros=null;
		Map <String,String> parametrosMap=null;
		String respServ=null;
		String login=null;
		JSONObject jsonObj=null;
		String jEnc=null;
		HashMap parametrosMC=null;
		String urlBase=null; 
		String servicio=null; 
		String protocol="http://";
		String cadenaOriginal=null;
		String cadenaDesencriptada=null;
		try{
			logger.info("Entro inicio llega : "+data);
			respuesta=new JSONObject();
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey_");
            if(!request.isSecure()){
            	respuesta.put("idError",200);
            	respuesta.put("errorMensaje", "Error, acceso no permitido HTTP");
            	respServ=respuesta.toString();
            }else if (keyResponse == null || keyResponse.length() == 0) {
            	respuesta.put("idError",1);
            	respuesta.put("errorMensaje", "Error al validar session");
            	respServ=respuesta.toString();
            } else {
            	parametros=AesCtr.decrypt(data, keyResponse, 256);
            	logger.info("Entro inicio llega : "+ parametros);
            	parametrosMap=UtilsService.splitQuery(parametros);
            	login=(String)parametrosMap.get("login");
            	 if (login != null && login.length() > 0) {
            		 if (request.isSecure()) {
                         protocol = "https://";
                     }
                     jsonObj = new JSONObject();
                     jsonObj.put("cadena", login);
                     logger.info(jsonObj.toString());
                     jEnc = AddcelCrypto.encryptHard(jsonObj.toString());
                     parametrosMC = new HashMap();
                     parametrosMC.put("json", jEnc);
                     urlBase = UrlProvider.urlbase;
                     servicio = UrlProvider.servicioRecuperaPassURL;
                     cadenaOriginal = ConsumeServicios.consumeServicio(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), parametrosMC);
                     if (cadenaOriginal != null && cadenaOriginal.length() > 0) {
                         logger.info("cadenaOriginal : " + cadenaOriginal);
                         cadenaDesencriptada = AddcelCrypto.decryptHard(cadenaOriginal);
                         logger.info("cadenaDesencriptada : " + cadenaDesencriptada);
                         if (cadenaDesencriptada != null && cadenaDesencriptada.equals("0")) {
                        	 respuesta.put("idError", 0);
                        	 respuesta.put("errorMensaje", "SE ENVIO UN CORREO CON SU NUEVA CONTRASEÑA");
                         } else {
                        	 respuesta.put("idError", 5);
                        	 respuesta.put("errorMensaje", "NO SE ENCONTRO AL USUARIO");
                         }
                     } else {
                    	 respuesta.put("idError", 4);
                    	 respuesta.put("errorMensaje", "ERROR EN LA OPERACION");
                     }
                 } else {
                	 respuesta.put("idError", 3);
                	 respuesta.put("errorMensaje", "ERROR AL RECIBIR DATOS");
                 }
            	respServ=AesCtr.encrypt(respuesta.toString(), keyResponse, 256);
            }
		}catch(Exception e){
			logger.info("Error en recuperaPass : ",e);
			respServ="{\"idError\":2,\"errorMensaje\":\"Error en la aplicaci&oacute;n, intente nuevamente\"}";
		}
		logger.info(respServ);
		return respServ;
	}
}
