package mx.mobilecard.adoservicios.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javacryption.aes.AesCtr;

import javax.servlet.http.HttpServletRequest;

import mx.mobilecard.adoservicios.model.vo.MapeoAdoVO;
import mx.mobilecard.adoservicios.model.vo.request.DatosPago;
import mx.mobilecard.adoservicios.services.AdoService;
import mx.mobilecard.adoservicios.services.ConsumeServicios;
import mx.mobilecard.adoservicios.utils.UrlProvider;
import mx.mobilecard.adoservicios.utils.UtilsService;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.addcel.utils.AddcelCrypto;

import crypto.Crypto;
@Controller
@SessionAttributes({"usuario","datosPago"})
public class RegistroController {
	private static final Logger logger = LoggerFactory.getLogger(RegistroController.class);
	@Autowired
	private AdoService adoPagoService;
	
	@RequestMapping(value = "/registro", method = RequestMethod.POST)
	public @ResponseBody String registro(@RequestParam("data") String data, HttpServletRequest request) {	
		String key=null;
		JSONObject respuesta=null;
		String parametros=null;
		Map <String,String> parametrosMap=null;
		String respServ=null;
		String login=null;
		JSONObject jsonObj=null;
		String jEnc=null;
		HashMap parametrosMC=null;
		String urlBase=null; 
		String servicio=null; 
		String protocol="http://";
		String cadenaOriginal=null;
		String cadenaDesencriptada=null;
        String flujo=null;
        DateFormat df = null;
        DatosPago datosPago=null;
        try{
        	 logger.info("Entro a registro");
        	 key = (String) request.getSession().getAttribute("jCryptionKey_");
        	 respuesta=new JSONObject();
        	 if(!request.isSecure()){
             	respuesta.put("idError",200);
             	respuesta.put("errorMensaje", "Error, acceso no permitido HTTP");
             	respServ=respuesta.toString();
             }else if (key == null || key.length() == 0) {
            	 respuesta.put("idError",1);
            	 respuesta.put("errorMensaje", "Error al validar session");
            	 respServ=respuesta.toString();
             } else {
            	parametros=AesCtr.decrypt(data, key, 256);
             	logger.info("Entro inicio llega : "+ parametros);
             	parametrosMap=UtilsService.splitQuery(parametros);
             	datosPago=(DatosPago)request.getSession().getAttribute("datosPago");
                 if (request.isSecure()) {
                     protocol = "https://";
                 }
                 df = new SimpleDateFormat("dd/MM/yyyy");
                 jsonObj = new JSONObject();
                 if (parametrosMap.get("login") != null) {
                     jsonObj.put("login", parametrosMap.get("login"));
                 }
                 if (parametrosMap.get("celular")!= null) {
                     jsonObj.put("telefono", parametrosMap.get("celular"));
                 }
                 if (parametrosMap.get("proveedor") != null) {
                     jsonObj.put("proveedor", Integer.parseInt(parametrosMap.get("proveedor")));
                 }
                 if (parametrosMap.get("email")!= null) {
                     jsonObj.put("mail", parametrosMap.get("email").toLowerCase());
                 }
                 if (parametrosMap.get("nombre")!= null) {
                     jsonObj.put("nombre", parametrosMap.get("nombre"));
                 }
                 if (parametrosMap.get("apellidoP") != null) {
                     jsonObj.put("apellido", parametrosMap.get("apellidoP"));
                 }
                 if (parametrosMap.get("apellidoM") != null) {
                     jsonObj.put("materno", parametrosMap.get("apellidoM"));
                 }
                 if (parametrosMap.get("fechaNac")  != null && parametrosMap.get("fechaNac").length() > 0) {
                     jsonObj.put("nacimiento", parametrosMap.get("fechaNac"));
                 }
                 jsonObj.put("registro", df.format(new Date()));
                 if (parametrosMap.get("sexo")  != null) {
                     jsonObj.put("sexo", parametrosMap.get("sexo") );
                 }
                 
                 if (parametrosMap.get("telefonoCasa") != null) {
                     jsonObj.put("tel_casa", parametrosMap.get("telefonoCasa"));
                 }
                 if (parametrosMap.get("telefonoOficina") != null) {
                     jsonObj.put("tel_oficina", parametrosMap.get("telefonoOficina"));
                 }
                 if (parametrosMap.get("estado")!= null) {
                     jsonObj.put("id_estado", Integer.parseInt(parametrosMap.get("estado")));
                 }
                 if (parametrosMap.get("ciudad") != null) {
                     jsonObj.put("ciudad", parametrosMap.get("ciudad"));
                 }
                 if (parametrosMap.get("calle") != null) {
                     jsonObj.put("calle", parametrosMap.get("calle"));
                 }
                 if (parametrosMap.get("numeroExt") != null) {
                     jsonObj.put("num_ext", Integer.parseInt(parametrosMap.get("numeroExt")));
                 }
                 if (parametrosMap.get("numeroInt") != null) {
                     jsonObj.put("num_interior", parametrosMap.get("numeroInt"));
                 }
                 if (parametrosMap.get("codigoPostal")!= null) {
                     jsonObj.put("cp", parametrosMap.get("codigoPostal"));
                 }
                 if (parametrosMap.get("tipoTarjeta") != null) {
                     jsonObj.put("tipotarjeta", Integer.parseInt(parametrosMap.get("tipoTarjeta")));
                 }
                 if (parametrosMap.get("numTarjeta") != null) {
                     jsonObj.put("tarjeta", parametrosMap.get("numTarjeta"));
                 }
                 if (parametrosMap.get("vigenciaTarjeta")!= null) {
                     jsonObj.put("vigencia", parametrosMap.get("vigenciaTarjeta"));
                 }
                 jsonObj.put("status", 1000);
                 if (datosPago.getImei() != null) {
                     jsonObj.put("imei", "ADO" + datosPago.getImei());
                 }

                 jsonObj.put("etiqueta", "");
                 jsonObj.put("numero", "");
                 jsonObj.put("tipo", parametrosMap.get("plataforma"));
                 jsonObj.put("software", "1.1.1");
                 jsonObj.put("modelo", "web");
                 jsonObj.put("terminos", "1");
                 if (datosPago.getImei() != null) {
                     jsonObj.put("key", datosPago.getImei());
                 }
                 urlBase = UrlProvider.urlbase;
                 servicio = UrlProvider.newUserURL;
                 jEnc = AddcelCrypto.encryptSensitive(UrlProvider.PASS_NEWUSER, jsonObj.toString());
                 logger.info(new StringBuilder(protocol).append(urlBase).append(servicio).append("?json=").append(jEnc).toString());
                 parametrosMC = new HashMap();
                 parametrosMC.put("json", jEnc);
                 cadenaOriginal = ConsumeServicios.consumeServicio(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), parametrosMC);
                 cadenaDesencriptada=Crypto.aesDecrypt(UrlProvider.parsePass(UrlProvider.PASS_NEWUSER), cadenaOriginal);
                 logger.info("Cadena desencriptada : {}",cadenaDesencriptada);
                 jsonObj = new JSONObject(cadenaDesencriptada);
                 if(jsonObj.getInt("resultado")==1){
                	 MapeoAdoVO mapeo=new MapeoAdoVO();
                	 mapeo.setEmail_ADO(datosPago.getEmail());
                	 mapeo.setLoginMobileCard(parametrosMap.get("login"));
                	 adoPagoService.insertaMapeo(mapeo);
                 }
                 respuesta.put("idError",(jsonObj.getInt("resultado")==1)?0:-1);
            	 respuesta.put("errorMensaje", jsonObj.get("mensaje"));
            	 respServ=AesCtr.encrypt(respuesta.toString(), key, 256);
             }
		}catch(Exception e){
			logger.info("Error en registro : ",e);
			respServ="{\"idError\":2,\"errorMensaje\":\"Error en la aplicaci&oacute;n, intente nuevamente\"}";
		}
		logger.info(respServ);
		return respServ;
	}
}
