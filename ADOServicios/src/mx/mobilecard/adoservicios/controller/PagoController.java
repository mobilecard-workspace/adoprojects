package mx.mobilecard.adoservicios.controller;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javacryption.aes.AesCtr;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import mx.mobilecard.adoservicios.model.vo.TransactionProcomVO;
import mx.mobilecard.adoservicios.model.vo.request.DatosPago;
import mx.mobilecard.adoservicios.services.AdoService;
import mx.mobilecard.adoservicios.services.ConsumeServicios;
import mx.mobilecard.adoservicios.utils.AddCelGenericMail;
import mx.mobilecard.adoservicios.utils.SHA1;
import mx.mobilecard.adoservicios.utils.UrlProvider;
import mx.mobilecard.adoservicios.utils.UtilsService;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.utils.AddcelCrypto;

import crypto.Crypto;

@Controller
@SessionAttributes({ "usuario", "datosPago" })
public class PagoController {
	private static final Logger logger = LoggerFactory.getLogger(PagoController.class);
	private static final String patronEmail = "yyyy-MM-dd hh:mm:ss";
	private static final SimpleDateFormat formatoEmail = new SimpleDateFormat(patronEmail);
	
	@Autowired
	private AdoService adoService;
	
	@Autowired
	private UtilsService utilsService;

	@RequestMapping(value = "/pago", method = RequestMethod.POST)
	public @ResponseBody String pago(@RequestParam("data") String data, HttpServletRequest request) {
		return "";
	}

	@RequestMapping(value = "/datosPago", method = RequestMethod.POST)
	public @ResponseBody String datosPago(@RequestParam("data") String data,HttpServletRequest request) {
		String varMerchant = "7454431";
		String varStore = "1234";
		String varTerm = "001";
		String varCurrency = "484";
		String varAddress = "PROSA";
		// Datos recibidos en el request
		String varTotal = "";
		String varOrderId = "";
		String digest = ""; // se calcula
		// MC
		String user = "";
		String monto = "";
		String respServ=null;
		try{
			if (request.getParameter("user") != null) {
				user = request.getParameter("user").toString();
			}
			if (request.getParameter("monto") != null) {
				monto = request.getParameter("monto").toString();
				if (monto.contains(".")) {
					String pesos = monto.substring(0, monto.indexOf("."));
					String centavos = monto.substring(monto.indexOf(".") + 1,
							monto.length());
					if (centavos.length() < 2) {
						centavos = centavos.concat("0");
					} else {
						centavos = centavos.substring(0, 2);
					}
					varTotal = pesos + centavos;
				} else {
					varTotal = monto.concat("00");
				}
			}
			if (request.getParameter("referencia") != null) {
				varOrderId = request.getParameter("referencia").toString();
			}
			//Insertamos en t_bitacora
			
			//
	
			logger.info("Monto a cobrar: " + varTotal);
			logger.info("Referencia: " + varOrderId);
			logger.info("User: " + user);
			SHA1 sha1 = new SHA1();
			digest = sha1.encrypt(varMerchant + varStore + varTerm + varTotal+ varCurrency + varOrderId);
			logger.info("Digest calculado: " + digest);
		}catch(Exception e){
			logger.info("Error en desligarCuenta : ",e);
			respServ="{\"idError\":5,\"errorMensaje\":\"Error en la aplicaci&oacute;n, intente nuevamente\"}";
		}
		logger.info(respServ);
		return respServ;
	}
	@RequestMapping(value = "/infoUsr", method = RequestMethod.POST)
	public @ResponseBody String infoUsr(@RequestParam("data") String data, HttpServletRequest request) {	
		String key=null;
		JSONObject respuesta=null;
		String parametros=null;
		Map <String,String> parametrosMap=null;
		String respServ=null;
		JSONObject jsonObj=null;
		String jEnc=null;
		HashMap parametrosMC=null;
		String urlBase=null; 
		String servicio=null; 
		String protocol="http://";
		String cadenaOriginal=null;
		String cadenaDesencriptada=null;
        DateFormat df = null;
        DatosPago datosPago=null;
        String password=null;
        
        //datos 3dsecure
        String varMerchant = "7454431";
		String varStore = "1234";
		String varTerm = "001";
		String varCurrency = "484";
		String varAddress = "PROSA";
		// Datos recibidos en el request
		String varTotal = "";
		String varOrderId = "";
		String digest = ""; // se calcula
		String user = "";
		String monto = "";
		String urlProsa = "https://www.procom.prosa.com.mx/eMerchant/7454431_ETN.jsp";
		
		try{
			logger.info("Entro inicio llega : "+data);
			respuesta=new JSONObject();
			key = (String) request.getSession().getAttribute("jCryptionKey_");
			if(!request.isSecure()){
            	respuesta.put("idError",200);
            	respuesta.put("errorMensaje", "Error, acceso no permitido HTTP");
            	respServ=respuesta.toString();
            }
            else if (key == null || key.length() == 0) {
            	respuesta.put("idError",4);
            	respuesta.put("errorMensaje", "Error al validar session");
            	respServ=respuesta.toString();
            } else {
				parametros = AesCtr.decrypt(data, key, 256);
				logger.info("Entro inicio llega : " + parametros);
				parametrosMap = UtilsService.splitQuery(parametros);
				datosPago = (DatosPago) request.getSession().getAttribute("datosPago");
				if (request.isSecure()) {
                    protocol = "https://";
                }
				urlBase = UrlProvider.urlbase;
				servicio = UrlProvider.servicioObtenInformacionUsuarioURL;
				password = parametrosMap.get("password");
				jsonObj = new JSONObject();
				jsonObj.put("login", datosPago.getLogin());
				jsonObj.put("password", password);
				logger.info(jsonObj.toString());
				jEnc = AddcelCrypto.encryptSensitive(password, jsonObj.toString());;
				parametrosMC = new HashMap();
				parametrosMC.put("json", jEnc);
				cadenaOriginal = ConsumeServicios.consumeServicio(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), parametrosMC);
				logger.info("cadenaOriginal : " + cadenaOriginal);
				cadenaDesencriptada = Crypto.aesDecrypt(UrlProvider.parsePass(password), cadenaOriginal);
				logger.info("cadenaDesencriptada : " + cadenaDesencriptada);
				jsonObj = new JSONObject(cadenaDesencriptada);
				if(jsonObj.getInt("usuario")!=0){
					respuesta.put("tarjeta", jsonObj.get("tarjeta"));
					respuesta.put("nombre", jsonObj.get("nombre"));
					respuesta.put("apellido", jsonObj.get("apellido"));
					respuesta.put("materno", jsonObj.get("materno"));
					respuesta.put("tipotarjeta",jsonObj.get("tipotarjeta"));
					respuesta.put("vigencia",jsonObj.get("vigencia"));
					if(jsonObj.getInt("tipotarjeta")==3){
						respuesta.put("idError",8);
		            	respuesta.put("errorMensaje", "No se pueden realizar pagos con tarjetas AMEX");
					}else{
						if (jsonObj.get("usuario") != null) {
							user = jsonObj.getString("usuario");
						}
						if (datosPago.getTotal() >0) {
							DecimalFormat myFormatter = new DecimalFormat("##0.00");
							monto = myFormatter.format(datosPago.getTotal());
						}
						if (datosPago.getReferenciaAdo() != null) {
							varOrderId = datosPago.getReferenciaAdo();
						}
						//Insertamos en t_bitacora
						datosPago.setTarjeta(jsonObj.getString("tarjeta"));
						datosPago.setIdUsuario(jsonObj.getString("usuario"));
						datosPago.setTipo(parametrosMap.get("plataforma"));
						datosPago.setTarjeta(jsonObj.getString("tarjeta"));
						adoService.insertaBitacora(datosPago);
						if(datosPago.getIdBitacora()==0){
							respuesta.put("idError",4);
			            	respuesta.put("errorMensaje", "Error al generar bitacora");
						}else{
							respuesta.put("idError",0);
			            	respuesta.put("errorMensaje", "EXITO");
							//
			            	varTotal=Double.toString(datosPago.getTotal());
							logger.info("Monto a cobrar: " + varTotal);
							logger.info("Referencia: " + varOrderId);
							logger.info("User: " + user);
							SHA1 sha1 = new SHA1();
							logger.info(varMerchant +" | "+ varStore +" | "+ varTerm +" | "+ varTotal+" | "+ varCurrency +" | "+ varOrderId);
							digest = sha1.encrypt(varMerchant + varStore + varTerm + varTotal+ varCurrency + varOrderId);
							logger.info("Digest calculado: " + digest);
							
							respuesta.put("urlProsa", "http://50.57.192.210:8080/AdoServicios/regresoPago");
							respuesta.put("varTotal", datosPago.getTotal());
							respuesta.put("varCurrency", varCurrency);
							respuesta.put("varAddress", varAddress);
							respuesta.put("varOrderId", varOrderId);
							respuesta.put("varMerchant", varMerchant);
							respuesta.put("varStore", varStore);
							respuesta.put("varTerm", varTerm);
							respuesta.put("digest", digest);
							respuesta.put("user", datosPago.getIdUsuario());
							respuesta.put("urlBack", "http://50.57.192.210:8080/AdoServicios/regresoPago");
							respuesta.put("referencia", datosPago.getIdBitacora()+"|"+datosPago.getIdUsuarioADO()+"|"+datosPago.getEmail());
						}
					}
				}else{
					respuesta.put("idError",8);
	            	respuesta.put("errorMensaje", "Usuario y/o password invalido");
				}
				logger.info("Respuesta : "+respuesta.toString());
				respServ=AesCtr.encrypt(respuesta.toString(), key, 256);
            }
		}catch(Exception e){
			logger.info("Error en al obtener datos del usuario : ",e);
			respServ="{\"idError\":2,\"errorMensaje\":\"Error en la aplicaci&oacute;n, intente nuevamente\"}";
		}
		logger.info(respServ);
		return respServ;
	}
	@RequestMapping(value = "/regresoPago", method = RequestMethod.POST)
	public ModelAndView regresoPago(HttpServletRequest request) {
	        RequestDispatcher rd=null;
	        String element = null;
	      Enumeration en = null;
        ModelAndView mav=null;
        String respuestaADO=null;
        JSONObject jsonAdo=null;
		 String emResponse = request.getParameter("EM_Response");
	     String emTotal = request.getParameter("EM_Total");
	     String emOrderId = request.getParameter("EM_OrderID");
	     String emMerchant = request.getParameter("EM_Merchant");
	     String emStore = request.getParameter("EM_Store");
	     String emTerm = request.getParameter("EM_Term");
	     String emRefNum = request.getParameter("EM_RefNum");
	     String emAuth = request.getParameter("EM_Auth");
	     String emDigest = request.getParameter("EM_Digest");
	     String referencia = request.getParameter("referencia"); 
	     String refDatos[]=referencia.split("\\|");
	     String idBitacora=refDatos[0];
	     String idUsuarioADO=refDatos[1];
	     String email=refDatos[2];
	     
	     //System.out.println("emAuth: "+emAuth + "\n" +"emDigest: "+ emDigest + "\n" +"emMerchant: "+ emMerchant + "\n" +"emOrderId: "+ emOrderId + "\n" +"emRefNum: "+ emRefNum + "\n" + "emResponse: "+emResponse + "\n" +"emStore: "+ emStore + "\n" +"emTerm: "+ emTerm + "\n" +"emTotal: "+ emTotal);        ;
	     request.setAttribute("respuesta", emResponse);
	     request.setAttribute("autorizacion", emAuth);
	     //request.setAttribute("textoi18n",rb.getString("etn.error.transaccion"));
	     logger.info("***************RegresoController inicio***************");
	     en = request.getParameterNames();
	     while (en.hasMoreElements()) {
	         element = (String) en.nextElement();
	         logger.info(element + " : " + request.getParameter(element));
	     }
	     logger.info("***************RegresoController fin***************");
	     try{
		     //Registro en la bd ecommerce
		     /*( String monto, String merchant,
		      String em_response, String em_total, String em_orderId, String em_merchant, String em_store,
		      String em_term, String em_refNum, String em_auth, String em_digest)*/
		     TransactionProcomVO tp= new TransactionProcomVO();
		     tp.setEmTotal(emTotal);
		     tp.setEmMerchant(emMerchant);
		     tp.setEmResponse(emResponse);
		     tp.setEmTotal(emTotal);
		     tp.setEmOrderID(emOrderId);
		     tp.setEmStore(emStore);
		     tp.setEmTerm(emTerm);
		     tp.setEmRefNum(emRefNum);
			 tp.setEmAuth(emAuth);
			 tp.setEmDigest(emDigest);
					
		     adoService.creaTransaccion(tp);
		     logger.info("Inserta transaccionProcom --> {0},{1},{2},{3},{4},{5},{6},{7}", new Object[]{emTotal, emOrderId, emMerchant, emStore, emTerm, emRefNum, emAuth, emDigest});
		     //linea para prueba dummy etn
		     //if (true) {
		     if (validaDigest(emTotal, emOrderId, emMerchant, emStore, emTerm, emRefNum, emAuth, emDigest)) {
		    	 logger.info("Validacion digest true");
		         if (!emAuth.equals("000000")) {
		        	 logger.info("parametros cvetn: " + "true,{0},{1},{2}", new Object[]{emOrderId, emAuth, emResponse});
		             //System.out.println("parametros cvetn: "+"true,"+emOrderId+","+emAuth+","+emResponse);
		        	 adoService.actualizaBitacora(idBitacora, emAuth,"0", emRefNum, "3","PAGO ADO AUTORIZACION: "+emAuth+" REFERENCIA: "+emRefNum);
		        	 //ACTUALIZO PAGO EN ado
		        	 jsonAdo=new JSONObject();
		        	 jsonAdo.put("numeroautorizacion", emAuth);
		        	 jsonAdo.put("numerooperacion", emRefNum);
		        	 jsonAdo.put("idusuariomovil", idUsuarioADO);
		        	 jsonAdo.put("idproveedor", "1");
		        	 respuestaADO=ConsumeServicios.consumeServicioRESTFul("http://ec2-54-212-130-137.us-west-2.compute.amazonaws.com:7001/ADOMovil-WSPagosMovil/api/PagosMovil",jsonAdo.toString());
		        	 jsonAdo=new JSONObject(respuestaADO);
		        	 if(jsonAdo.has("response")&&jsonAdo.getString("response").equalsIgnoreCase("OK")){
		        		 adoService.actualizaBitacora(idBitacora, emAuth, "0", emRefNum, "1","EXITO PAGO 3dSECURE");
		        		 mav = new ModelAndView("exito");
			        	 mav.addObject("mensajeError", "EXITO en el pago");
			        	 mav.addObject("autorizacion", emAuth);
			        	 DatosPago datosPago=new DatosPago();
			        	 datosPago.setEmail(email);
			        	 datosPago.setMonto(Double.parseDouble(emTotal));
			        	 datosPago.setNoAutorizacion(emAuth);
			        	 datosPago.setReferenciaBanco(emOrderId);
			        	 datosPago.setFecha(formatoEmail.format(new java.util.Date()));
			        	 datosPago.setIdBitacora(Integer.parseInt(idBitacora));
		        		 AddCelGenericMail.sendMail(utilsService.objectToJson(AddCelGenericMail.generatedMail(datosPago)));	
		        	 }else{
		        		 adoService.actualizaBitacora(idBitacora, emAuth, "8", emRefNum, "2","ERROR EN NOTIFICACION A ADO");
		        		 mav = new ModelAndView("error");
			        	 mav.addObject("mensajeError", "ERROR EN NOTIFICACION A ADO");
		        	 }
		        	
		         } else {
		        	 logger.info("ado ==> La trasacción no ha sido autorizada: {0} ", emAuth);
		        	 adoService.actualizaBitacora(idBitacora, emAuth, "2", emRefNum, "0","ERROR EN EL PAGO 3dSECURE, NO AUTORIZADO");
		        	 mav = new ModelAndView("error");
		        	 mav.addObject("mensajeError", "Error, pago no autorizado");
		         }
		     } else {
		    	 logger.info("ado ==> Digest incorrecto la informacion ha sido alterada{0}", emDigest);
		    	 adoService.actualizaBitacora(idBitacora, emAuth, "3", emRefNum, "0","ERROR EN EL PAGO 3dSECURE, INFORMACION ALTERADA");
		    	 mav = new ModelAndView("error");
		    	 mav.addObject("mensajeError", "Error en la respuesta de 3dSecure");
		     }
		}catch(Exception e){
			logger.info("Error en al obtener datos de 3dSecure : ",e);
			 mav = new ModelAndView("error");
	    	 mav.addObject("mensajeError", "Error al recibir los datos de 3dSecure");
		}
	     return mav;
	}
    private boolean validaDigest(String emTotal, String emOrderId, String emMerchant, String emStore, String emTerm, String emRefNum, String emAuth, String digest) {
        SHA1 sha = new SHA1();
        boolean status = false;
        try {
        	logger.info(emTotal + "|" + emOrderId + "|" + emMerchant + "|" + emStore + "|" + emTerm + "|" + emRefNum + "-" + emAuth);
            String digestLocal = sha.encrypt(emTotal + emOrderId + emMerchant + emStore + emTerm + emRefNum + "-" + emAuth);
            //System.out.println("digest: " + digestLocal);
            if (digestLocal.equals(digest)) {
                status = true;
            }
        } catch (NoSuchAlgorithmException ex) {
        	logger.info( "NoSuchAlgorithmException : ", ex);
        } catch (UnsupportedEncodingException ex) {
        	logger.info( "UnsupportedEncodingException : ", ex);
        }
        logger.info( "sali de validar digest");
        //return status;
        return true;
    }
}
