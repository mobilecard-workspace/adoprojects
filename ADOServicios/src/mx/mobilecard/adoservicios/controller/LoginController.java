package mx.mobilecard.adoservicios.controller;

import java.util.Map;

import javacryption.aes.AesCtr;

import javax.servlet.http.HttpServletRequest;

import mx.mobilecard.adoservicios.model.vo.MapeoAdoVO;
import mx.mobilecard.adoservicios.model.vo.request.DatosPago;
import mx.mobilecard.adoservicios.services.AdoService;
import mx.mobilecard.adoservicios.utils.UtilsService;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes({"usuario","datosPago"})
public class LoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	@Autowired
	private AdoService adoPagoService;

	@RequestMapping(value = "/loginMC", method = RequestMethod.POST)
	public @ResponseBody String loginMC(@RequestParam("data") String data, HttpServletRequest request) {	
		String keyResponse=null;
		JSONObject respuesta=null;
		String parametros=null;
		Map <String,String> parametrosMap=null;
		DatosPago datosPago=null;
		Object loginResp[]=null;
		String respServ=null;
		MapeoAdoVO mapeoAdo=null;
		try{
			logger.info("Entro inicio llega : "+data);
			respuesta=new JSONObject();
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey_");
            if(!request.isSecure()){
            	respuesta.put("idError",200);
            	respuesta.put("errorMensaje", "Error, acceso no permitido HTTP");
            	respServ=respuesta.toString();
            }
            else if (keyResponse == null || keyResponse.length() == 0) {
            	respuesta.put("idError",4);
            	respuesta.put("errorMensaje", "Error al validar session");
            	respServ=respuesta.toString();
            } else {
            	parametros=AesCtr.decrypt(data, keyResponse, 256);
            	logger.info("Entro inicio llega : "+ parametros);
            	parametrosMap=UtilsService.splitQuery(parametros);
            	datosPago=(DatosPago)request.getSession().getAttribute("datosPago");
            	loginResp=adoPagoService.validacionLogin(datosPago.getImei(),parametrosMap.get("login"),parametrosMap.get("password"),false);
            	datosPago.setPassword(parametrosMap.get("password"));
            	datosPago.setLogin(parametrosMap.get("login"));
            	mapeoAdo=adoPagoService.existeMapeo(datosPago.getEmail());
            	if(mapeoAdo==null&&((Integer)loginResp[0])==0){
            		MapeoAdoVO mapeo=new MapeoAdoVO();
	               	mapeo.setEmail_ADO(datosPago.getEmail());
	               	mapeo.setLoginMobileCard(parametrosMap.get("login"));
	               	adoPagoService.insertaMapeo(mapeo);
            	}
            	respuesta.put("idError", loginResp[0]);
            	respuesta.put("errorMensaje", loginResp[1]);
            	respServ=AesCtr.encrypt(respuesta.toString(), keyResponse, 256);
            }
		}catch(Exception e){
			logger.info("Error en loginMC : ",e);
			respServ="{\"idError\":5,\"errorMensaje\":\"Error en la aplicaci&oacute;n, intente nuevamente\"}";
		}
		logger.info(respServ);
		return respServ;
	}
	
	
	@RequestMapping(value = "/validarLogin", method = RequestMethod.POST)
	public @ResponseBody String validarLogin(@RequestParam("data") String data, HttpServletRequest request) {	
		String keyResponse=null;
		JSONObject respuesta=null;
		String parametros=null;
		Map <String,String> parametrosMap=null;
		DatosPago datosPago=null;
		String loginResp=null;
		String respServ=null;
		try{
			logger.info("Entro inicio llega : "+data);
			respuesta=new JSONObject();
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey_");
            if(!request.isSecure()){
            	respuesta.put("idError",200);
            	respuesta.put("errorMensaje", "Error, acceso no permitido HTTP");
            	respServ=respuesta.toString();
            }else if (keyResponse == null || keyResponse.length() == 0) {
            	respuesta.put("idError",4);
            	respuesta.put("errorMensaje", "Error al validar session");
            	respServ=respuesta.toString();
            } else {
            	parametros=AesCtr.decrypt(data, keyResponse, 256);
            	logger.info("Entro inicio llega : "+ parametros);
            	parametrosMap=UtilsService.splitQuery(parametros);
            	datosPago=(DatosPago)request.getSession().getAttribute("datosPago");
            	loginResp=adoPagoService.validaLogin(parametrosMap.get("login"),datosPago.getLogin());
            	if(loginResp!=null&&loginResp.length()>0){
	            	respuesta.put("idError", -1);
	            	respuesta.put("errorMensaje", "EL LOGIN PROPORCIONADO YA ESTA REGISTRADO");	
            	}else{
            		respuesta.put("idError", 0);
                	respuesta.put("errorMensaje", "LOGIN VALIDO");
            	}
            	respServ=AesCtr.encrypt(respuesta.toString(), keyResponse, 256);
            }
		}catch(Exception e){
			logger.info("Error en validarLogin : ",e);
			respServ="{\"idError\":5,\"errorMensaje\":\"Error en la aplicaci&oacute;n, intente nuevamente\"}";
		}
		logger.info(respServ);
		return respServ;
	}
	
	@RequestMapping(value = "/validarDN", method = RequestMethod.POST)
	public @ResponseBody String validarDN(@RequestParam("data") String data, HttpServletRequest request) {	
		String keyResponse=null;
		JSONObject respuesta=null;
		String parametros=null;
		Map <String,String> parametrosMap=null;
		DatosPago datosPago=null;
		String dnResp=null;
		String respServ=null;
		try{
			logger.info("Entro inicio llega : "+data);
			respuesta=new JSONObject();
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey_");
            if(!request.isSecure()){
            	respuesta.put("idError",200);
            	respuesta.put("errorMensaje", "Error, acceso no permitido HTTP");
            	respServ=respuesta.toString();
            }else if (keyResponse == null || keyResponse.length() == 0) {
            	respuesta.put("idError",4);
            	respuesta.put("errorMensaje", "Error al validar session");
            	respServ=respuesta.toString();
            } else {
            	parametros=AesCtr.decrypt(data, keyResponse, 256);
            	logger.info("Entro inicio llega : "+ parametros);
            	parametrosMap=UtilsService.splitQuery(parametros);
            	datosPago=(DatosPago)request.getSession().getAttribute("datosPago");
            	dnResp=adoPagoService.validaDN(parametrosMap.get("dn"),datosPago.getLogin());
            	if(dnResp!=null&&dnResp.length()>0){
	            	respuesta.put("idError", -1);
	            	respuesta.put("errorMensaje", "EL NÚMERO PROPORCIONADO YA ESTA REGISTRADO");	
            	}else{
            		respuesta.put("idError", 0);
                	respuesta.put("errorMensaje", "NÚMERO VALIDO");
            	}
            	respServ=AesCtr.encrypt(respuesta.toString(), keyResponse, 256);
            }
		}catch(Exception e){
			logger.info("Error en validarDN : ",e);
			respServ="{\"idError\":5,\"errorMensaje\":\"Error en la aplicaci&oacute;n, intente nuevamente\"}";
		}
		logger.info(respServ);
		return respServ;
	}
	
	@RequestMapping(value = "/validarEmail", method = RequestMethod.POST)
	public @ResponseBody String validarEmail(@RequestParam("data") String data, HttpServletRequest request) {	
		String keyResponse=null;
		JSONObject respuesta=null;
		String parametros=null;
		Map <String,String> parametrosMap=null;
		DatosPago datosPago=null;
		String emailResp=null;
		String respServ=null;
		try{
			logger.info("Entro inicio llega : "+data);
			respuesta=new JSONObject();
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey_");
            if(!request.isSecure()){
            	respuesta.put("idError",200);
            	respuesta.put("errorMensaje", "Error, acceso no permitido HTTP");
            	respServ=respuesta.toString();
            }else if (keyResponse == null || keyResponse.length() == 0) {
            	respuesta.put("idError",4);
            	respuesta.put("errorMensaje", "Error al validar session");
            	respServ=respuesta.toString();
            } else {
            	parametros=AesCtr.decrypt(data, keyResponse, 256);
            	logger.info("Entro inicio llega : "+ parametros);
            	parametrosMap=UtilsService.splitQuery(parametros);
            	datosPago=(DatosPago)request.getSession().getAttribute("datosPago");
            	emailResp=adoPagoService.validaEmail(parametrosMap.get("email"),datosPago.getLogin());
            	if(emailResp!=null&&emailResp.length()>0){
	            	respuesta.put("idError", -1);
	            	respuesta.put("errorMensaje", "EL EMAIL PROPORCIONADO YA ESTA REGISTRADO");	
            	}else{
            		respuesta.put("idError", 0);
                	respuesta.put("errorMensaje", "EMAIL VALIDO");
            	}
            	respServ=AesCtr.encrypt(respuesta.toString(), keyResponse, 256);
            }
		}catch(Exception e){
			logger.info("Error en validarEmail : ",e);
			respServ="{\"idError\":5,\"errorMensaje\":\"Error en la aplicaci&oacute;n, intente nuevamente\"}";
		}
		logger.info(respServ);
		return respServ;
	}
	
	@RequestMapping(value = "/validarTDC", method = RequestMethod.POST)
	public @ResponseBody String validarTDC(@RequestParam("data") String data, HttpServletRequest request) {	
		String keyResponse=null;
		JSONObject respuesta=null;
		String parametros=null;
		Map <String,String> parametrosMap=null;
		DatosPago datosPago=null;
		String tdcResp=null;
		String respServ=null;
		try{
			logger.info("Entro inicio llega : "+data);
			respuesta=new JSONObject();
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey_");
            if(!request.isSecure()){
            	respuesta.put("idError",200);
            	respuesta.put("errorMensaje", "Error, acceso no permitido HTTP");
            	respServ=respuesta.toString();
            }else if (keyResponse == null || keyResponse.length() == 0) {
            	respuesta.put("idError",4);
            	respuesta.put("errorMensaje", "Error al validar session");
            	respServ=respuesta.toString();
            } else {
            	parametros=AesCtr.decrypt(data, keyResponse, 256);
            	logger.info("Entro inicio llega : "+ parametros);
            	parametrosMap=UtilsService.splitQuery(parametros);
            	datosPago=(DatosPago)request.getSession().getAttribute("datosPago");
            	tdcResp=adoPagoService.validaTDC(parametrosMap.get("numTarjeta"),datosPago.getLogin());
            	if(tdcResp!=null&&tdcResp.length()>0){
	            	respuesta.put("idError", -1);
	            	respuesta.put("errorMensaje", "LA TARJETA PROPORCIONADA YA ESTA REGISTRADA");	
            	}else{
            		respuesta.put("idError", 0);
                	respuesta.put("errorMensaje", "TARJETA VALIDA");
            	}
            	respServ=AesCtr.encrypt(respuesta.toString(), keyResponse, 256);
            }
		}catch(Exception e){
			logger.info("Error en validarEmail : ",e);
			respServ="{\"idError\":5,\"errorMensaje\":\"Error en la aplicaci&oacute;n, intente nuevamente\"}";
		}
		logger.info(respServ);
		return respServ;
	}
}
