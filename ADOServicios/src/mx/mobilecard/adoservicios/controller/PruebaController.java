package mx.mobilecard.adoservicios.controller;

import java.util.HashMap;

import mx.mobilecard.adoservicios.services.AdoService;
import mx.mobilecard.adoservicios.services.ConsumeServicios;
import mx.mobilecard.adoservicios.utils.UtilsService;
import mx.mobilecard.crypto.CryptoAddcel;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import crypto.Crypto;

@Controller
@SessionAttributes({"usuario","datosPago"})
public class PruebaController {
	private static final Logger logger = LoggerFactory.getLogger(PruebaController.class);
	@Autowired
	private AdoService adoPagoService;
	
	@RequestMapping(value = "/prueba")
	public @ResponseBody String prueba() {	
		JSONObject jsonObjEnvia = new JSONObject();
		JSONObject jsonObjRecibe = new JSONObject();
		String respuesta=null;
		HashMap parametros=null;
		String jEnc=null;
		StringBuffer htmlRespuesta=new StringBuffer();
		String jsonAdo=" {\"idComercio\":"+UtilsService.proveedor+",\"idProducto\":\""+UtilsService.producto+"\",\"idConcepto\":"+UtilsService.idConcepto+",\"concepto\":\"Compra,descripcion\",\"valor\":222.00, \"email\":\"scanfernan17@gmail.com\",\"imei\":\"123456789012345\",\"securecode\":\""+UtilsService.securecode+"\",\"certificado\":\"MHdOr6cmhxiY3lKJ37PjbJnYBSXPwA7IH8qMz+qgB3Q=\",\"idUsuarioMovil\":\"2390\",\"idTransaccion\":\"200006206580\"}";
		//String jsonAdo="{\"concepto\":\"Compra,descripcion\",\"idUsuarioMovil\":\"562\",\"idComercio\":1,\"token\":\"BICZBvMKJgvvDgNlr1jnSVm+quKMeMhpP0D4nPPbeY4=\",\"email\":\"has@has.com\",\"valor\":680,\"imei\":\"358092050408488\",\"idTransaccion\":\"200006208591\",\"idProducto\":\"1001\",\"securecode\":\"gd432jg3\",\"idConcepto\":1001,\"certificado\":\"73a9ed7f5f31642d87aabafc7775a96a\"}";
		try{
			jsonObjEnvia = new JSONObject();
	        jsonObjEnvia.put("idComercio", UtilsService.proveedor);
			jsonObjEnvia.put("usuario", UtilsService.userToken);
			jsonObjEnvia.put("password", UtilsService.passToken);
			jEnc = Crypto.aesEncrypt(UtilsService.pass,jsonObjEnvia.toString());
	        parametros = new HashMap();
	        parametros.put("json", jEnc);
	        respuesta=ConsumeServicios.consumeServicio("https://www.mobilecard.mx:8443/ADOServicios/getToken",parametros);
	        //respuesta=consumeServicio("http://localhost:8080/ADOServicios/getToken",parametros);
	        logger.info("respuesta : " +respuesta);
	        respuesta=Crypto.aesDecrypt(UtilsService.passR,respuesta);
	        jsonObjRecibe = new JSONObject(respuesta);
	        jsonObjEnvia = new JSONObject(jsonAdo);
	        StringBuilder certificadoLocal=new StringBuilder();
			certificadoLocal.append(UtilsService.proveedor);
			certificadoLocal.append("^");
			certificadoLocal.append(UtilsService.idConcepto);
			certificadoLocal.append("^");
			certificadoLocal.append(UtilsService.securecode);
			certificadoLocal.append("^");
			certificadoLocal.append(jsonObjRecibe.get("token"));
			jsonObjEnvia.put("certificado", CryptoAddcel.md5Base64(certificadoLocal.toString()));
	        jsonObjEnvia.put("token",jsonObjRecibe.get("token"));
	        System.out.println(jsonObjEnvia.toString());
	        jEnc =  Crypto.aesEncrypt(UtilsService.pass,jsonObjEnvia.toString());
	       // parametros.put("json", "sSvIceoS%2FUKNoQA7d1Or1Jr0EdvTNzHy7%2FeWiZrC2Ls%3D");
	        parametros = new HashMap();
	        parametros.put("json", jEnc);
	        //respuesta=ConsumeServicios.consumeServicio("http://localhost:8080/ADOServicios/login",parametros);
	        //logger.info(respuesta);
	        htmlRespuesta.append("<hrml><head><script>function envia(){document.myForm.submit();}</script></head><body onload=\"envia()\">");
	        htmlRespuesta.append("<form name=\"myForm\" action=\"https://www.mobilecard.mx/ADOServicios/login\" method=\"post\">");
	        htmlRespuesta.append("<input type=\"hidden\" name=\"json\" value=\""+jEnc+"\">");
	        htmlRespuesta.append("</form></body>");
	        htmlRespuesta.append("</hrml>");
        }catch(Exception e){
        	logger.error("Error : ", e);
        }
        return htmlRespuesta.toString();
	}
}
