package mx.mobilecard.adoservicios.model.mapper;

import java.util.HashMap;

import mx.mobilecard.adoservicios.model.vo.MapeoAdoVO;

import org.apache.ibatis.annotations.Param;


public interface AdoMapper {
	
	public double obtenComision(@Param(value = "idProveedor") String idProveedor);
	
	public String montoMaximoPago();

	public String getFechaActual();

	public int difFechaMin(String fechaToken);
	
	public String obtenParametro(String idParametro);
	
	public String obtenSecuencia(String idParametro);
	
	public MapeoAdoVO mapeoADO(String email);
	
	public int eliminaMapeoADO(String login);
	
	public int insertaMapeoADO(MapeoAdoVO parametros);
	
	public int actualizaMapeoADO(MapeoAdoVO parametros);
	
	public String validaEmail(String email);
	
	public String validaTelefono(String dn);
	
	public String validaTDC(String tdc);
	
	public String validaLogin(String login);
	
	public String validaIMEI(String imei);
	
	public String validaEstatusLogin(String login);
	
}
