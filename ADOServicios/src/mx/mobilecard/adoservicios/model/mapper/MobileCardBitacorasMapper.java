package mx.mobilecard.adoservicios.model.mapper;



import mx.mobilecard.adoservicios.model.vo.BitacoraProsaVO;
import mx.mobilecard.adoservicios.model.vo.BitacoraVO;
import mx.mobilecard.adoservicios.model.vo.ProductoVO;
import mx.mobilecard.adoservicios.model.vo.TransactionProcomVO;

public interface MobileCardBitacorasMapper {
	public int insertaBitacora(BitacoraVO bitacora);
	public int insertaBitacoraProsa(BitacoraProsaVO bitacora);
	public int actualizaBitacora(BitacoraVO bitacora);
	public int actualizaBitacoraProsa(BitacoraProsaVO bitacora);
	public ProductoVO datosCargo(String idProducto);
	public String seleccionaParametros(String clave);
	public String seleccionaErrorProsa(String idError);
	public int insertaTransaccionProcom(TransactionProcomVO transactionProcomVO);
}
