package mx.mobilecard.adoservicios.model.vo;

public class BitacoraProsaVO {
	private String id_bitacoraProsa;
	private String id_bitacora;
	private String id_usuario;
	private String tarjeta;
	private String transaccion;
	private String autorizacion;
	private java.sql.Date fecha;
	private java.sql.Date bit_hora;
	private String Concepto;
	private String TarjetaIAVE;
	private String Cargo;
	private String comision;
	private String cx;
	private String cy;
	public String getId_bitacoraProsa() {
		return id_bitacoraProsa;
	}
	public void setId_bitacoraProsa(String id_bitacoraProsa) {
		this.id_bitacoraProsa = id_bitacoraProsa;
	}
	public String getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(String id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getTransaccion() {
		return transaccion;
	}
	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public java.sql.Date getFecha() {
		return fecha;
	}
	public void setFecha(java.sql.Date fecha) {
		this.fecha = fecha;
	}
	public java.sql.Date getBit_hora() {
		return bit_hora;
	}
	public void setBit_hora(java.sql.Date bit_hora) {
		this.bit_hora = bit_hora;
	}
	public String getConcepto() {
		return Concepto;
	}
	public void setConcepto(String concepto) {
		Concepto = concepto;
	}
	public String getTarjetaIAVE() {
		return TarjetaIAVE;
	}
	public void setTarjetaIAVE(String tarjetaIAVE) {
		TarjetaIAVE = tarjetaIAVE;
	}
	public String getCargo() {
		return Cargo;
	}
	public void setCargo(String cargo) {
		Cargo = cargo;
	}
	public String getComision() {
		return comision;
	}
	public void setComision(String comision) {
		this.comision = comision;
	}
	public String getCx() {
		return cx;
	}
	public void setCx(String cx) {
		this.cx = cx;
	}
	public String getCy() {
		return cy;
	}
	public void setCy(String cy) {
		this.cy = cy;
	}
}
