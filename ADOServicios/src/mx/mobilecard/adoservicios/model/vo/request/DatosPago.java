package mx.mobilecard.adoservicios.model.vo.request;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DatosPago implements Serializable {

	private static final long serialVersionUID = 217828031301781865L;
	
	private int idBitacora;
	private String login;
	private String idUsuario;
	private String password;
	
	private String email;
	private String nombre;
	private String producto;
	private double monto;
	private String token;
	private String imei;
	private String tipo;
	private String software;
	private String modelo;
	private String cx;
	private String cy;
	private String key;

	private String tarjetaMask;
	private String tarjeta;
	private String vigencia;
	private String cvv2;
	private String id_tipo_tarjeta;
	
	private String referenciaBanco;
	private String referenciaAdo;
	private String noAutorizacion;
	private String mensajeError;
	private double comision;
	
	private String msg;
	private String transaccion;

	
	private String status;
	private String fecha;
	private int error;
	private double total;
	private String idUsuarioADO;
	
	
	
	public String getIdUsuarioADO() {
		return idUsuarioADO;
	}
	public void setIdUsuarioADO(String idUsuarioADO) {
		this.idUsuarioADO = idUsuarioADO;
	}
	public String getId_tipo_tarjeta() {
		return id_tipo_tarjeta;
	}
	public void setId_tipo_tarjeta(String id_tipo_tarjeta) {
		this.id_tipo_tarjeta = id_tipo_tarjeta;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public int getError() {
		return error;
	}
	public void setError(int error) {
		this.error = error;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getTarjetaMask() {
		return tarjetaMask;
	}
	public void setTarjetaMask(String tarjetaMask) {
		this.tarjetaMask = tarjetaMask;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getNoAutorizacion() {
		return noAutorizacion;
	}
	public void setNoAutorizacion(String noAutorizacion) {
		this.noAutorizacion = noAutorizacion;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getCx() {
		return cx;
	}
	public void setCx(String cx) {
		this.cx = cx;
	}
	public String getCy() {
		return cy;
	}
	public void setCy(String cy) {
		this.cy = cy;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getTransaccion() {
		return transaccion;
	}
	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}
	public String getReferenciaBanco() {
		return referenciaBanco;
	}
	public void setReferenciaBanco(String referenciaBanco) {
		this.referenciaBanco = referenciaBanco;
	}
	public String getReferenciaAdo() {
		return referenciaAdo;
	}
	public void setReferenciaAdo(String referenciaAdo) {
		this.referenciaAdo = referenciaAdo;
	}



}
