package mx.mobilecard.adoservicios.model.vo.request;

public class RespuestaPago {
	private String tipoDonacion;
	private String monto;
	private String tdcM;
	private String autorizacion;
	private String addcelR;
	private String resultado;
	private String mensajeError;
	
	public String getTipoDonacion() {
		return tipoDonacion;
	}
	public void setTipoDonacion(String tipoDonacion) {
		this.tipoDonacion = tipoDonacion;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getTdcM() {
		return tdcM;
	}
	public void setTdcM(String tdcM) {
		this.tdcM = tdcM;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getAddcelR() {
		return addcelR;
	}
	public void setAddcelR(String addcelR) {
		this.addcelR = addcelR;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	
}
