package mx.mobilecard.adoservicios.model.vo;

public class ProductoVO {
	private String id_producto;
	private String pro_clave;
	private String pro_monto;
	private String pro_path;
	private String id_proveedor;
	private String nombre;
	
	public String getId_producto() {
		return id_producto;
	}
	public void setId_producto(String id_producto) {
		this.id_producto = id_producto;
	}
	public String getPro_clave() {
		return pro_clave;
	}
	public void setPro_clave(String pro_clave) {
		this.pro_clave = pro_clave;
	}
	public String getPro_monto() {
		return pro_monto;
	}
	public void setPro_monto(String pro_monto) {
		this.pro_monto = pro_monto;
	}
	public String getPro_path() {
		return pro_path;
	}
	public void setPro_path(String pro_path) {
		this.pro_path = pro_path;
	}
	public String getId_proveedor() {
		return id_proveedor;
	}
	public void setId_proveedor(String id_proveedor) {
		this.id_proveedor = id_proveedor;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
