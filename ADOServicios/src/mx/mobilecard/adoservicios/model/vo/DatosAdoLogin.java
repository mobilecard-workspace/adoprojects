package mx.mobilecard.adoservicios.model.vo;

import mx.mobilecard.adoservicios.utils.UtilsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatosAdoLogin {
	private static final Logger logger = LoggerFactory.getLogger(DatosAdoLogin.class);
	private int idComercio;
	private String idProducto;
	private int idConcepto;
	private String concepto;
	private double valor;
	private String idTransaccion;
	private String email;
	private String imei;
	private String securecode;
	private String certificado;
	private String token;
	private String idUsuarioMovil;

	public String getIdUsuarioMovil() {
		return idUsuarioMovil;
	}
	public void setIdUsuarioMovil(String idUsuarioMovil) {
		this.idUsuarioMovil = idUsuarioMovil;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public int getIdComercio() {
		return idComercio;
	}
	public void setIdComercio(int idComercio) {
		this.idComercio = idComercio;
	}
	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public int getIdConcepto() {
		return idConcepto;
	}
	public void setIdConcepto(int idConcepto) {
		this.idConcepto = idConcepto;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public String getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getSecurecode() {
		return securecode;
	}
	public void setSecurecode(String securecode) {
		this.securecode = securecode;
	}
	public String getCertificado() {
		return certificado;
	}
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
	public boolean validate(){
		if(idComercio==0){logger.info("idcomercio invalido");return false;}
		if(idProducto==null||idProducto.length()==0){logger.info("idProducto invalido");return false;}
		if(idConcepto==0){logger.info("idConcepto invalido");return false;}
		if(concepto==null||concepto.length()==0){logger.info("concepto invalido");return false;}
		if(valor==0){logger.info("valor invalido");return false;}
		if(idTransaccion==null||idTransaccion.length()==0){logger.info("idTransaccion invalido");return false;}
		if(email==null||email.length()==0){logger.info("email invalido");return false;}
		if(imei==null||imei.length()==0){logger.info("imei invalido");return false;}
		if(securecode==null||securecode.length()==0){logger.info("securecode invalido");return false;}
		if(certificado==null||certificado.length()==0){logger.info("certificado invalido");return false;}
		if(token==null||token.length()==0){logger.info("token invalido");return false;}
		if(idUsuarioMovil==null||idUsuarioMovil.length()==0){logger.info("idUsuarioMovil invalido");return false;}
		return true;
	}
	
}
