package mx.mobilecard.adoservicios.model.vo;

public class RespuestaServicioVO {
		private int idError;
		private String msgError;
		private Object datos;
		public int getIdError() {
			return idError;
		}
		public void setIdError(int idError) {
			this.idError = idError;
		}
		public String getMsgError() {
			return msgError;
		}
		public void setMsgError(String msgError) {
			this.msgError = msgError;
		}
		public Object getDatos() {
			return datos;
		}
		public void setDatos(Object datos) {
			this.datos = datos;
		}
		
	}
