package mx.mobilecard.adoservicios.model.vo;

public class MapeoAdoVO {
	private String email_ADO; 
	private String loginMobileCard;
	private int estatus;
	public String getEmail_ADO() {
		return email_ADO;
	}
	public void setEmail_ADO(String email_ADO) {
		this.email_ADO = email_ADO;
	}
	public String getLoginMobileCard() {
		return loginMobileCard;
	}
	public void setLoginMobileCard(String loginMobileCard) {
		this.loginMobileCard = loginMobileCard;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
}
