package mx.mobilecard.adoservicios.model.vo;

import java.sql.Date;

public class BitacoraVO {
	private String id_bitacora;
	private String id_usuario;
	private String id_proveedor;
	private String id_producto;
	private Date bit_fecha;
	private Date bit_hora;
	private String bit_concepto;
	private String bit_cargo;
	private String bit_ticket;
	private String bit_no_autorizacion;
	private String bit_codigo_error;
	private String bit_card_id;
	private String bit_status;
	private String imei;
	private String destino;
	private String tarjeta_compra;
	private String tipo;
	private String software;
	private String modelo;
	private String wkey;
	private String pase;
	
	public String getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(String id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getId_proveedor() {
		return id_proveedor;
	}
	public void setId_proveedor(String id_proveedor) {
		this.id_proveedor = id_proveedor;
	}
	public String getId_producto() {
		return id_producto;
	}
	public void setId_producto(String id_producto) {
		this.id_producto = id_producto;
	}
	public Date getBit_fecha() {
		return bit_fecha;
	}
	public void setBit_fecha(Date bit_fecha) {
		this.bit_fecha = bit_fecha;
	}
	public Date getBit_hora() {
		return bit_hora;
	}
	public void setBit_hora(Date bit_hora) {
		this.bit_hora = bit_hora;
	}
	public String getBit_concepto() {
		return bit_concepto;
	}
	public void setBit_concepto(String bit_concepto) {
		this.bit_concepto = bit_concepto;
	}
	public String getBit_cargo() {
		return bit_cargo;
	}
	public void setBit_cargo(String bit_cargo) {
		this.bit_cargo = bit_cargo;
	}
	public String getBit_ticket() {
		return bit_ticket;
	}
	public void setBit_ticket(String bit_ticket) {
		this.bit_ticket = bit_ticket;
	}
	public String getBit_no_autorizacion() {
		return bit_no_autorizacion;
	}
	public void setBit_no_autorizacion(String bit_no_autorizacion) {
		this.bit_no_autorizacion = bit_no_autorizacion;
	}
	public String getBit_codigo_error() {
		return bit_codigo_error;
	}
	public void setBit_codigo_error(String bit_codigo_error) {
		this.bit_codigo_error = bit_codigo_error;
	}
	public String getBit_card_id() {
		return bit_card_id;
	}
	public void setBit_card_id(String bit_card_id) {
		this.bit_card_id = bit_card_id;
	}
	public String getBit_status() {
		return bit_status;
	}
	public void setBit_status(String bit_status) {
		this.bit_status = bit_status;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getTarjeta_compra() {
		return tarjeta_compra;
	}
	public void setTarjeta_compra(String tarjeta_compra) {
		this.tarjeta_compra = tarjeta_compra;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getWkey() {
		return wkey;
	}
	public void setWkey(String wkey) {
		this.wkey = wkey;
	}
	public String getPase() {
		return pase;
	}
	public void setPase(String pase) {
		this.pase = pase;
	}
	
}
