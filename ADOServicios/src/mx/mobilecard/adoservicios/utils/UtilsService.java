package mx.mobilecard.adoservicios.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UtilsService {
	//QA
	//public static final String pass="147D2A61D200F770C78EA6D825500B96";
	//public static final String passR="37DF3F8A8D3AE809CAFB177FA1C967A2";
	//PROD
	public static final String pass="2D66D45707C73398CF1F54CFB14F0585";
	public static final String passR="CAA00149A75AD0C3D3A31EEBB26F67A5";

	public static final String TDC_PASS="5525963513";
	public static final int proveedor=28;
	public static final String producto="119";
	public static final int idConcepto=965;
	public static final String securecode="aeVvTqPs";
	public static final String userToken="ClienteADO";
	public static final String passToken="Ah3FtnBT";
	
	private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	@Autowired
	private ObjectMapper mapperJk;	
	
	public <T> String objectToJson(T object){
		String json=null;
		try {
			json=mapperJk.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			logger.error("1.- Error al generar json: {}",e);
		} catch (JsonMappingException e) {
			logger.error("2.- Error al mapperar objeccto: {}",e);
		} catch (IOException e) {
			logger.error("3.- Error al generar json: {}",e);
		}
		return json;
	}

	public <T> Object jsonToObject(String json, Class<T> clase) {		
		Object obj = null;
		try {
			obj = mapperJk.readValue(json, clase);
		} catch (JsonParseException e) {
			logger.error("Error al parsear cadena json 1: {}", e);
		} catch (JsonMappingException e) {
			logger.error("Error al parsear cadena json 2: {}", e);
		} catch (IOException e) {
			logger.error("Error al parsear cadena json 3: {}", e);
		}
		return obj;
	}
	
	public String peticionUrlPostParams(String urlRemota,String parametros){
		String respuesta = null;
		//http://50.57.192.210:8080/Conversor/currency?fromCurrency=usd&toCurrency=MXN
		URL url;
		try {
			url = new URL(urlRemota);		
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		// Se indica que se escribira en la conexi�n
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		// Se escribe los parametros enviados a la url
		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());		
		wr.write(parametros);
		wr.close();			
		if(con.getResponseCode() == HttpURLConnection.HTTP_OK ||
				con.getResponseCode() == HttpURLConnection.HTTP_ACCEPTED){			
			BufferedReader br = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String jsonString;
			while ((jsonString = br.readLine()) != null) {
				sb.append(jsonString);
			}
			respuesta=sb.toString();			
		}else{
			respuesta = "ERROR";
		}
		con.disconnect();
		} catch (MalformedURLException e) {
			logger.error("ERROR URL malformada: {}",e);
		} catch (IOException e) {
			logger.error("ERROR IO : {}",e);
		}
		return respuesta;
	}
	
	public static boolean validaCodigoBarras(String cuenta){
		int valor=0;
		String valorStr=null;
		int arrayValores[]=null;
		int sumatoria=0;
		int residuo=0;
		int dv=0;
		if(cuenta.length()<26)return false;
		if(!cuenta.startsWith("17"))return false;
		arrayValores=new int[25];
		logger.info("Validando codigo de barras : "+cuenta);
		for(int i=0;i<cuenta.length()-1;i++){
			valor=Character.getNumericValue(cuenta.charAt(i));
			if(i%2==0){
				valor*=2;
			}
			if(valor>9){
				valorStr=Integer.toString(valor);
				valor=0;
				for(int j=0;j<valorStr.length();j++){
					valor+=Character.getNumericValue(valorStr.charAt(j));
				}
			}
			arrayValores[i]=valor;
		}
		for(int i=0;i<arrayValores.length;i++){
			sumatoria+=arrayValores[i];
		}
		logger.info("sumatoria : "+sumatoria);
		residuo=sumatoria%10;
		logger.info("residuo : "+residuo);
		if(residuo>0){
			dv=10-residuo;
		}
		logger.info("dv : "+dv);
		if(cuenta.endsWith(Integer.toString(dv))){
			return true;
		}else{
			return false;
		}
	}
	public static Map<String, String> splitQuery(String query) throws UnsupportedEncodingException {
	    Map<String, String> query_pairs = new LinkedHashMap<String, String>();
	    String[] pairs = query.split("&");
	    for (String pair : pairs) {
	        int idx = pair.indexOf("=");
	        query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
	    }
	    return query_pairs;
	}
}
