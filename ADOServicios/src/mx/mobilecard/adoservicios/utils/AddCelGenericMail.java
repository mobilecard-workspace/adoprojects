package mx.mobilecard.adoservicios.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.mobilecard.adoservicios.model.vo.CorreoVO;
import mx.mobilecard.adoservicios.model.vo.request.DatosPago;

public class AddCelGenericMail {
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	//private static final String urlString = "http://localhost:8080/MailSenderAddcel/enviaCorreoAddcel";
	private static final String urlString = "http://50.57.192.214:8080/MailSenderAddcel/enviaCorreoAddcel";
	
	private static final String HTML_DOBY = " <html><head> <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">  <style type=\"text/css\">  body { background: none repeat scroll 0 0 #FFFFFF; font-family: 'Open Sans', sans-serif;  } table.info_description{ height: auto; width: 620px; position: relative; } table.info_description td{ height: auto; color: #DF1D44; font-size: 30px; line-height: 32px; text-align: center; display: inline-block; vertical-align: top; *display: inline; *zoom:1; } section.main_info{ background: none repeat scroll 0 0 #CBCBCB; width: 100%; height: auto; margin: 0 auto; background:#CBCBCB; }  table.donate{ width: 620px; }  table.donate p.text_donation_centers{ color: #73021E; font-size: 18px; font-style:italic; line-height: 20px; text-align: center; } table.donate p.text_donation_centers span.highlights{ font-weight: bold; }  table.block{ width: 400px; height: auto; color: #232323; } table.block td.title_rigth{ font-weight: normal; text-transform:uppercase; font-size: 12px; line-height: 16px; text-align: right; width: 170px; } table.block td.address_center{ margin: 0 0 18px; font-weight: bold; font-style:italic; font-size: 14px; line-height: 14px; text-align: center; } section.title_tamaulipas{ width: 100%; height: auto; position: relative; } section.title_tamaulipas p.state_tamaulipas{ margin: 12px 0 18px 0; color: #C30042; font-size: 14px; font-weight: bold; text-transform:uppercase; line-height: 16px; text-align: center; } section.title_tamaulipas div.line_tamaulipas{ width: 250px; height: 1px; padding: 6px 0; position: absolute; top: 8px; left: 50%; margin-left:-300px; border-top: 1px #9E9E9E solid; } section.title_tamaulipas div.line_tamaulipas_right{ width: 250px; height: 1px; padding: 6px 0; position: absolute; top: 8px; left: 50%; margin-left:50px; border-top: 1px #9E9E9E solid; } section.helpers{ background: none repeat scroll 0 0 #FFFFFF; width: 940px; height: auto; position: relative; padding: 26px 0 0; margin: 0 auto; vertical-align: center; color: #000000; font-weight: bold; text-transform:uppercase; font-size: 12px; line-height: 14px; text-align: center; } section.helpers div.extra_text_helpers{ margin: 44px auto 20px;  } section.helpers div.footer_text_helpers{ margin: 0 0 0 0;  } </style> </head> <body data-twttr-rendered=\"true\">  <table class=\"info_description\"  align=\"center\"> <tbody> <tr> <td><img src=\"cid:identifierCID00\"></td> </tr>  </tbody> </table>  <section class=\"main_info\">  <table class=\"donate\"  align=\"center\"> <tbody> <tr> <td> <p class=\"text_donation_centers\"> <span class=\"highlights\">#ADO</span> Gracias por su pago. </p> </td> </tr> </tbody> </table> <section class=\"title_tamaulipas\" align= \"center\" > <div class=\"line_tamaulipas\"></div> <p class=\"state_tamaulipas\">Pago ADO</p> <div class=\"line_tamaulipas_right\"></div> </section>  <table class=\"block\"  align=\"center\" cellpadding=\"2\"> <tbody> <tr> <td class=\"title_rigth\"> Monto: </td> <td class=\"address_center\"> <#MONTO#> </td> <td class=\"address_center\"> </td> </tr> <tr> <td class=\"title_rigth\"> Moneda: </td> <td class=\"address_center\"> <#MONEDA#> </td> <td width=\"50\"> </td> </tr> <tr> <td class=\"title_rigth\"> Fecha: </td> <td class=\"address_center\"> <#FECHA#> </td> <td > </td> </tr> <tr> <td class=\"title_rigth\"> Autorizacion Bancaria: </td> <td class=\"address_center\"> <#AUTBAN#> </td> <td > </td> </tr> <tr> <td class=\"title_rigth\"> Referencia: </td> <td class=\"address_center\"> <#REFE#> </td> <td > </td> </tr> <tr> <td > </td> <td > </td> <td > </td> </tr> </tbody> </table> </section>  <section class=\"helpers\"> <div class=\"extra_text_helpers\"> Nota. Este correo es de car&aacute;ter informativo, no es necesario que responda al mismo. </div> <div class=\"footer_text_helpers\"> Gracias por usar <img src=\"cid:identifierCID01\"> </div> </section>  </body> </html>";
			

	public static CorreoVO generatedMail(DatosPago datosPagoVO){
		logger.info("Genera objeto para envio de mail: " + datosPagoVO.getEmail());
		
		CorreoVO correo = new CorreoVO();
		try{
	//		String[] attachments = {src_file};
			String body = HTML_DOBY.toString();
			body = body.replaceAll("<#PRODUCTO#>", "Pago ADO");
			body = body.replaceAll("<#MONTO#>", Double.toString(datosPagoVO.getMonto()));
			body = body.replaceAll("<#MONEDA#>", "MXN");
			body = body.replaceAll("<#FECHA#>", (datosPagoVO.getFecha()!= null ? datosPagoVO.getFecha().substring(0, 19) : ""));
			body = body.replaceAll("<#AUTBAN#>", datosPagoVO.getNoAutorizacion()!= null ? datosPagoVO.getNoAutorizacion() : "");
			body = body.replaceAll("<#REFE#>", Integer.toString(datosPagoVO.getIdBitacora()));
			//body = body.replaceAll("<#VOUCHER#>", String.valueOf(datosPagoVO.getVoucher()));
			
			String from = "no-reply@addcel.com";
			String subject = "Acuse pago ADO - Referencia: " + datosPagoVO.getReferenciaBanco();
			String[] to = {datosPagoVO.getEmail()};
			String[] cid = {
					"/usr/java/resources/images/Ado/logo_ado.jpg",
					"/usr/java/resources/images/Ado/logoMC.png"
					};
	
	//		correo.setAttachments(attachments);
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			
//			sendMail(correo);
//			logger.info("Fin proceso de envio email");
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}

	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			logger.info("data = " + data);

			URL url = new URL(urlString);
			logger.info("Conectando con " + urlString);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
