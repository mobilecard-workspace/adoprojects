package mx.mobilecard.adoservicios.utils;

public class UrlProvider {
    public static final String PASS_ADDCEL= "1234567890ABCDEF0123456789ABCDEF";
    public static final String PASS_NEWUSER="mCL8m39sJ1";
    public static final String PASS_ENCTDC="5525963513";
	// public static final String
	//public static final String urlbase="www.mobilecard.mx:8080/AddCelBridge/Servicios/";
	public static final String urlbase = "www.mobilecard.mx:8443/AddCelBridge/Servicios/";
	public static final String servicioCatalogoEstadosURL = "adc_getEstados.jsp";
	public static final String loginUrl = "adc_userLogin.jsp";
	public static final String newUserURL = "adc_userInsert.jsp";
	public static final String userUpdateURL = "adc_userUpdate.jsp";
	public static final String userPassUpdateURL = "adc_userPasswordUpdate.jsp";
	public static final String userDataURL = "adc_getUserData.jsp";
	public static final String servicioCatalogoTarjetasURL = "adc_getCardType.jsp";
	public static final String servicioCatalogoBancosURL = "adc_getBanks.jsp";
	public static final String servicioCatalogoProvidersURL = "adc_getProviders.jsp";
	public static final String servicioCatalogoCategoriasURL = "adc_getCategoris.jsp";
	public static final String servicioCatalogoProductosURL = "adc_getProducts.jsp";
	//public static final String servicioCatalogoCategoriasURL = "dc_getUserPurchases.jsp";
	public static final String servicioCompraIAVE = "adc_purchase_iave.jsp";
	public static final String servicioCondicionesURL = "adc_getConditions.jsp";
	public static final String servicioRecuperaPassURL = "adc_RecoveryUP.jsp";
	public static final String servicioCambiaPassURL = "adc_userPasswordUpdate.jsp";
	public static final String servicioObtenInformacionUsuarioURL = "adc_getUserData.jsp";
	public static final String servicioActualizaInformacionUsuarioURL = "adc_userUpdate.jsp";
	
    public static String parsePass(String pass) {
        int len = pass.length();
        String key = "";
        for (int i = 0; i < 32 / len; i++) {
            key += pass;
        }
        int carry = 0;
        while (key.length() < 32) {
            key += pass.charAt(carry);
            carry++;
        }
        return key;
    }

}
