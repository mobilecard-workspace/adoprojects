<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META NAME="HandheldFriendly" content="True">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<style>
@font-face{
   font-family: 'DIN regular'; src:url('./resources/DINPro-Regular.otf')format('opentype');
 }
.block-ui {
    cursor: wait;
    position: absolute;
    top: 0px;
    left: 0px;  
    width: 100%;
    height: 100%;
    background-color: #CCCCCC;
	font-size: 100%;
	font-family: DIN regular;
}

.center {
	position: absolute;
	top:0;
	left:0;
	right:0;
	bottom:0;
	margin: auto;
	width:90%;
	height:90%;
	-moz-border-radius: 15px;
	border-radius: 15px;
	background-color:#c60c30;
	color:#ffffff;
	text-align:center;
}
</style>
</head>
<body>
<div class="block-ui">
	<div class="center">
		<span style="font-size:30pt;">PAGO REALIZADO</span><br/><br/>
		<span style="font-size:24pt; height:200px;overflow:scroll;">${mensajeError}</span><br/>
		<input type="hidden" value="${autorizacion}" id="autorizacion">
		<input type="hidden" value="${mensajeError}" id="mensajeError">
	</div>
</div>
</body>
</html>
