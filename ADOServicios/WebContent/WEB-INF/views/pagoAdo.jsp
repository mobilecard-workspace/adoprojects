<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<META NAME="HandheldFriendly" content="True">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" href="<c:url value="/resources/css/jquery.mobile.structure-1.4.2.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/ADOTheme.min.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/jquery.mobile.icons.min.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/mobiscroll.animation.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/mobiscroll.scroller.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/mobiscroll.scroller.android-ics.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/css/adoMovil.css"/>" type="text/css" />

<script>
var mensajeError3421="${mensajeError}";
var pantalla3421="${pantalla}";
var email="${email}";
var idError="${idError}";
var loginSugerido="${loginSugerido}";


function goBack(){
	var activePage = $.mobile.activePage.attr("id");
	switch(activePage){
	case "pago":
	case "welcome":
		break;
	case "recuperaPass":
	case "pagelogin":
		if($("#flujoLogin").val()==='L'){	
			$.mobile.changePage('#welcome');
		}else if($("#flujoLogin").val()==='A'){
			$.mobile.changePage('#pago');
		}
		else if($("#flujoLogin").val()==='D'){
			$.mobile.changePage('#pago');
		}
		break;
	case "editaDatos":
	case "modificarPass":
		$.mobile.changePage('#pago');
		break;	
	case "adicionales1":
		if($("#flujoAdicionales").val()=='R'){
			$.mobile.changePage('#welcome');
		}else if($("#flujoAdicionales").val()=='U'){
			$.mobile.changePage('#pago');
		}
		break;
	case "adicionales2":
			$.mobile.changePage('#adicionales1');
		break;
	}
	
}
</script>
<script src="<c:url value="/resources/js/jquery-1.10.2.min.js"/>"  type="text/javascript"></script>
<script src="<c:url value="/resources/js/jquery.jcryption.min.js"/>"  type="text/javascript"></script>
<script src="<c:url value="/resources/js/jquery.validate.min.js"/>"  type="text/javascript"></script>
<script src="<c:url value="/resources/js/additional-methods.min.js"/>"  type="text/javascript"></script>
<script src="<c:url value="/resources/js/jquery.blockUI.js"/>"  type="text/javascript"></script>
<script	src="<c:url value="/resources/js/jquery.mobile-1.4.2.min.js"/>"  type="text/javascript"></script>
<script	src="<c:url value="/resources/js/mobiscroll.core.js"/>"  type="text/javascript"></script>
<script	src="<c:url value="/resources/js/mobiscroll.scroller.js"/>"  type="text/javascript"></script>
<script src="<c:url value="/resources/js/mobiscroll.scroller.android-ics.js"/>"  type="text/javascript"></script>
<script src="<c:url value="/resources/js/mobiscroll.datetime.js"/>"  type="text/javascript"></script>
<script	src="<c:url value="/resources/js/mobiscroll.select.js"/>"  type="text/javascript"></script>
<script src="<c:url value="/resources/js/i18n/mobiscroll.i18n.es.js"/>"  type="text/javascript"></script>
<script src="<c:url value="/resources/js/adoMovil.js"/>"  type="text/javascript"></script>
<script>      
</script>

<title>Insert title here</title>

</head>
<body>
<div data-role="page" id="pago">
	<form name="frmPago" id="frmPago" autocomplete="off">
		<div data-role="header" data-position="fixed" data-tap-toggle="false" style="width:100%;background-color:#5E5F60">
				<h1>PAGO</h1>
			<a href="#editaDatos" id="btnConfig" style="margin-top:5px; margin-right:5px;height:25px !important;width:50px !important;line-height:30px;border:none !important; background-color:transparent" class="ui-btn ui-btn-right ui-nodisc-icon ui-btn-icon-notext ui-icon-Ado-gear "></a>
		</div>
		<div data-role="content">
			<label style="font-weight:bold;">LOGIN:</label>
			<label style="font-weight:bold;text-indent: 5em;" id="loginCompra">${login}</label>
			<label style="font-weight:bold;">CONCEPTO:</label>
			<label style="font-weight:bold;text-indent: 5em;">${concepto}</label>
			<label style="font-weight:bold;">MONTO:</label>
			<label style="font-weight:bold;text-indent: 5em;">${monto}</label>
			<label style="font-weight:bold;">COMISION:</label>
			<label style="font-weight:bold;text-indent: 5em;">${comision}</label>
			<label for="password">CONTRASEÑA<span style="color:#c60c30">*</span>:</label>
			<input type="password" id="password" name="password" maxlength="12"/>
		</div>
		<div data-role="footer" data-position="fixed" data-tap-toggle="false" style="background-color:#c60c30;color:#ffffff;cursor:pointer"  onclick="pagar3dSecure()">
			<h4>PAGAR</h4>
		</div>
	</form>
</div>

<div data-role="page" id="welcome" data-title="Bienvenido">
	<div data-role="content">
		<div class="imagenMC" id="logoW">
		</div>
		<a href="#pagelogin" id="btnLogin" class="ui-btn ui-corner-all ui-shadow" style="background-color:#c60c30;color:#ffffff" >USUARIO MOBILECARD</a>
		<a href="#" id="btnRegistro" class="ui-btn ui-corner-all ui-shadow" style="background-color:#c60c30;color:#ffffff" >REGISTRARSE</a>
	</div>
</div>


<div data-role="page" id="pagelogin" data-title="Login">
	<form name="frmPageLogin" id="frmPageLogin" autocomplete="off">
		<div data-role="header" data-position="fixed" data-tap-toggle="false"  style="width:100%;background-color:#5E5F60;">
		<a href="" id="goBackButton" style="margin-top:5px; margin-right:5px;height:25px !important;width:30px !important;line-height:30px;border:none !important; background-color:transparent" class="ui-btn ui-btn-left ui-nodisc-icon ui-btn-icon-notext ui-icon-Ado-back "></a>
			<h1>LOGIN</h1>
		</div>
		<div data-role="content">
			<label for="login">LOGIN<span style="color:#c60c30">*</span>:</label>
			<input type="text" name="login" id="login" maxlength="40" value="${login}"/>
			<label for="password">CONTRASEÑA<span style="color:#c60c30">*</span>:</label>
			<input type="password" name="password" id="password"  maxlength="12" />
			<label>&nbsp;</label>
			<div class="ui-grid-a">
				<div class="ui-block-a"></div>
				<div class="ui-block-b" style="text-align:center"><a href="#recuperaPass"  style="text-decoration: none;color:#5E5F60">¿OLVIDASTE TU CONTRASEÑA?</a></div>
			</div><!-- /grid-a -->	
			<div data-role="footer" data-position="fixed"  id="botonLogin" data-tap-toggle="false" style="background-color:#c60c30;color:#ffffff;cursor:pointer">
				<h4>ENVIAR</h4>
			</div>
		</div>
	</form>
</div>

<div data-role="page" id="recuperaPass" data-title="RECUPERA CONTRASEÑA">

	<form name="frmPageRecupera" id="frmPageRecupera" autocomplete="off">
		<div data-role="header" data-position="fixed" data-tap-toggle="false"  style="width:100%;background-color:#5E5F60;">
			<a href="" id="goBackButton" style="margin-top:5px; margin-right:5px;height:25px !important;width:30px !important;line-height:30px;border:none !important; background-color:transparent" class="ui-btn ui-btn-left ui-nodisc-icon ui-btn-icon-notext ui-icon-Ado-back "></a>
			<h1>RECUPERA CONTRASEÑA</h1>
		</div>
		<div data-role="content">
			<label for="login">LOGIN<span style="color:#c60c30">*</span>:</label>
			<input type="text" name="login" id="login" maxlength="40" value="${login}"/>	
			<div data-role="footer" data-position="fixed"  data-tap-toggle="false" style="background-color:#c60c30;color:#ffffff;cursor:pointer"  onclick="recuperaPass()">
				<h4>ENVIAR</h4>
			</div>
		</div>
	</form>
</div>


<div data-role="page" id="modificarPass" data-title="MODIFICAR CONTRASEÑA">
	<form name="frmModificaPass" id="frmModificaPass" autocomplete="off">
		<div data-role="header" data-position="fixed" data-tap-toggle="false"  style="width:100%;background-color:#5E5F60;">
			<a href="" id="goBackButton" style="margin-top:5px; margin-right:5px;height:25px !important;width:30px !important;line-height:30px;border:none !important; background-color:transparent" class="ui-btn ui-btn-left ui-nodisc-icon ui-btn-icon-notext ui-icon-Ado-back "></a>
			<h1>CAMBIAR CONTRASEÑA</h1>
		</div>
		<div data-role="content">
			<label for="login">LOGIN<span style="color:#c60c30">*</span>:</label>
			<input type="text" name="login" id="login" maxlength="40" value="${login}"/>
			<label for="oldPassword">CONTRASEÑA ACTUAL<span style="color:#c60c30">*</span>:</label>
			<input type="password" name="oldPassword" id="oldPassword"  maxlength="12" />
			<label for="newPassword">NUEVA CONTRASEÑA<span style="color:#c60c30">*</span>:</label>
			<input type="password" name="newPassword" id="newPassword"  maxlength="12" />
			<label for="confPassword">REPETIR CONTRASEÑA<span style="color:#c60c30">*</span>:</label>
			<input type="password" name="confPassword" id="confPassword"  maxlength="12" />
			<div data-role="footer" data-position="fixed"  data-tap-toggle="false" style="background-color:#c60c30;color:#ffffff;cursor:pointer"  onclick="cambiaPass()">
				<h4>ENVIAR</h4>
			</div>
		</div>
	</form>
</div>



<div data-role="page" id="editaDatos">
	<div data-role="header" data-position="fixed"  data-tap-toggle="false" style="width:100%;background-color:#5E5F60">
		<a href="" id="goBackButton" style="margin-top:5px; margin-right:5px;height:25px !important;width:30px !important;line-height:30px;border:none !important; background-color:transparent" class="ui-btn ui-btn-left ui-nodisc-icon ui-btn-icon-notext ui-icon-Ado-back "></a>
		<h1>EDITAR DATOS</h1>
	</div>
	<div data-role="content">
		<div class="imagenMC" id="logoE"></div>
		<a href="#" id="btnDesligarCuentas" class="ui-btn ui-corner-all ui-shadow" style="background-color:#c60c30;color:#ffffff" >DESLIGAR CUENTAS</a>
		<a href="#modificarPass" id="btnCambiaPass" class="ui-btn ui-corner-all ui-shadow" style="background-color:#c60c30;color:#ffffff" >MODIFICAR CONTRASEÑA</a>
		<a href="#recuperaPass" id="btnResetearPass" class="ui-btn ui-corner-all ui-shadow" style="background-color:#c60c30;color:#ffffff" >OLVIDE MI CONTRASEÑA</a>
		<a href="#" id="btnActualizarRegistro" class="ui-btn ui-corner-all ui-shadow" style="background-color:#c60c30;color:#ffffff" >ACTUALIZAR REGISTRO</a>
	</div>
</div>




<div data-role="page" id="adicionales1">
	<form name="frmAdicionales1" id="frmAdicionales1" autocomplete="off">
		<div data-role="header" data-position="fixed"  data-tap-toggle="false"  style="width:100%;background-color:#5E5F60">
			<a href="" id="goBackButton" style="margin-top:5px; margin-right:5px;height:25px !important;width:30px !important;line-height:30px;border:none !important; background-color:transparent" class="ui-btn ui-btn-left ui-nodisc-icon ui-btn-icon-notext ui-icon-Ado-back "></a>
			<h1>REGISTRO 1/2</h1>
		</div>
		<div data-role="content">
			<label for="login">LOGIN<span style="color:#c60c30">*</span>:</label>
			<input type="text" name="login" id="login" maxlength="40"  onblur="validaLogin(this)">
			<label for="celular">NÚMERO CELULAR<span style="color:#c60c30">*</span>:</label>
			<input type="tel" name="celular" id="celular" maxlength="10" onblur="validaDN(this)">
			<label for="confcelular">CONFIRMAR NÚMERO CELULAR:</label>
			<input type="tel" name="confcelular" id="confcelular" maxlength="10">
			<label for="proveedor" >PROVEEDOR<span style="color:#c60c30">*</span>:</label>	
			<select id="proveedor" name="proveedor" data-role="none">
				<option value="1">IUSACELL</option>
				<option value="2">MOVISTAR</option>
				<option value="3">TELCEL</option>
				<option value="4">UNEFON</option>
			</select>
			<label for="email">CORREO ELECTRONICO<span style="color:#c60c30">*</span>:</label>
			<input type="email" name="email" id="email" maxlength="100" onblur="validaEmail(this)" value="${email}">
			<label for="confemail">CONFIRMAR CORREO ELECTRONICO:</label>
			<input type="email" name="confemail" id="confemail" maxlength="100" value="${email}">
			<label for="nombre">NOMBRE(S)<span style="color:#c60c30">*</span>:</label>
			<input type="text" name="nombre" id="nombre" maxlength="40">
			<label for="apellidoP">APELLIDO PATERNO<span style="color:#c60c30">*</span>:</label>
			<input type="text" name="apellidoP" id="apellidoP" maxlength="50">
			<label for="apellidoM">APELLIDO MATERNO<span style="color:#c60c30">*</span>:</label>
			<input type="text" name="apellidoM" id="apellidoM" maxlength="50">
			<label for="fechaNac">FECHA DE NACIMIENTO<span style="color:#c60c30">*</span>:</label>	
			<input type="text" id="fechaNac" name="fechaNac" placeholder="AÑO - MES - DIA" readonly>
			<label for="sexo">SEXO<span style="color:#c60c30">*</span>:</label>	
			<select id="sexo" name="sexo" data-role="none">
				<option value="M">MASCULINO</option>
				<option value="F">FEMENINO</option>
			</select>
			<label for="telefonoCasa">TELEFONO DE CASA:</label>	
			<input type="tel" id="telefonoCasa" name="telefonoCasa" maxlength="10">
			<label for="telefonoOficina">TELEFONO DE OFICINA</label>	
			<input type="tel" id="telefonoOficina" name="telefonoOficina" maxlength="10">
			<label>&nbsp;</label>
			<div data-role="footer" data-position="fixed"  id="adicionales1Continua" data-tap-toggle="false" style="background-color:#c60c30;color:#ffffff;cursor:pointer" >
				<h4>CONTINUAR</h4>
			</div>
		</div>
	</form>
</div>

<div data-role="page" id="adicionales2">
	<form name="frmAdicionales2" id="frmAdicionales2" autocomplete="off">
		<div data-role="header" data-position="fixed"  data-tap-toggle="false"  style="width:100%;background-color:#5E5F60">
			<a href="" id="goBackButton" style="margin-top:5px; margin-right:5px;height:25px !important;width:30px !important;line-height:30px;border:none !important; background-color:transparent" class="ui-btn ui-btn-left ui-nodisc-icon ui-btn-icon-notext ui-icon-Ado-back"></a>
			<h1>REGISTRO 2/2</h1>
		</div>
		<div data-role="content">
			<label for="estado" >ESTADO<span style="color:#c60c30">*</span>:</label>	
			<select id="estado" name="estado" data-role="none">
				<option value="1">AGUASCALIENTES</option>
				<option value="2">BAJA CALIFORNIA</option>
				<option value="3">BAJA CALIFORNIA SUR</option>
				<option value="4">CAMPECHE</option>
				<option value="5">COAHUILA</option>
				<option value="6">COLIMA</option>
				<option value="7">CHIAPAS</option>
				<option value="8">CHIHUAHUA</option>
				<option value="9">DISTRITO FEDERAL</option>
				<option value="10">DURANGO</option>
				<option value="11">GUANAJUATO</option>
				<option value="12">GUERRERO</option>
				<option value="13">HIDALGO</option>
				<option value="14">JALISCO</option>
				<option value="15">ESTADO DE MEXICO</option>
				<option value="16">MICHOACAN</option>
				<option value="17">MORELOS</option>
				<option value="18">NAYARIT</option>
				<option value="19">NUEVO LEON</option>
				<option value="20">OAXACA</option>
				<option value="21">PUEBLA</option>
				<option value="22">QUERETARO</option>
				<option value="23">QUINTANA ROO</option>
				<option value="24">SAN LUIS POTOSI</option>
				<option value="25">SINALOA</option>
				<option value="26">SONORA</option>
				<option value="27">TABASCO</option>
				<option value="28">TAMAULIPAS</option>
				<option value="29">TLAXCALA</option>
				<option value="30">VERACRUZ</option>
				<option value="31">YUCATAN</option>
				<option value="32">ZACATECAS</option>
			</select>
			<label for="ciudad">CIUDAD:</label>
			<input type="text" name="ciudad" id="ciudad" maxlength="50">
			<label for="calle">CALLE:</label>
			<input type="text" name="calle" id="calle" maxlength="50">
			<label for="numeroExt">NÚMERO EXTERIOR:</label>
			<input type="number" name="numeroExt" id="numeroExt" maxlength="11">
			<label for="numeroInt">NÚMERO INTERIOR:</label>
			<input type="text" name="numeroInt" id="numeroInt" maxlength="20">
			<label for="codigoPostal">CÓDIGO POSTAL:</label>
			<input type="number" name="codigoPostal" id="codigoPostal" maxlength="11">
			<label for="tipoTarjeta" >TIPO DE TARJETA<span style="color:#c60c30">*</span>:</label>	
			<select id="tipoTarjeta" name="tipoTarjeta" data-role="none">
				<option value="1">VISA</option>
				<option value="2">MASTERCARD</option>
			</select>
			<label for="numTarjeta">NÚMERO DE TARJETA<span style="color:#c60c30">*</span>:</label>
			<input type="text" name="numTarjeta" id="numTarjeta" maxlength="16" onblur="validaTDC(this)">
			<label for="vigenciaTarjeta">VIGENCIA<span style="color:#c60c30">*</span>:</label>	
			<input type="text" id="vigenciaTarjeta" name="vigenciaTarjeta" placeholder="MES / AÑO" readonly>
			<label>&nbsp;</label>
			<a href="#terminos" class="ui-btn ui-corner-all ui-shadow" style="background-color:#c60c30;color:#ffffff" >TERMINOS Y CONDICIONES</a>
			<fieldset class="ui-grid-a">
				<div class="ui-block-a">
					<select name="terminos" id="terminos" data-role="slider"  >
						<option value="0">NO</option>
						<option value="1">SI</option>
					</select>
				</div>
				<div class="ui-block-b"><label>ACEPTO LOS TÉRMINOS<br/>Y CONDICIONES<span style="color:#c60c30">*</span></label></div>	   
			</fieldset>
			<input type="hidden" id="plataforma" name="plataforma" value=""/>
			<label>&nbsp;</label>
			
			<div data-role="footer" id="adicionales2Continua" data-position="fixed" data-tap-toggle="false" style="background-color:#c60c30;color:#ffffff;cursor:pointer"  onclick="$.mobile.changePage('#adicionales2')">
				<h4>ENVIAR</h4>
			</div>
		</div>
	</form>
</div>
<div data-role="page" id="terminos">
	<form name="frmTerminos" id="frmTerminos" autocomplete="off">
		<div data-role="header" data-position="fixed" data-tap-toggle="false" style="width:100%;background-color:#5E5F60">
			<h1>TERMINOS Y CONDICIONES</h1>
		</div>
		<div data-role="content" class="ui-content">
			<p style="text-align:left">TERMINOS Y CONDICIONES DEL SERVICIO. <br/> <br/>PRIMERA.- Radiocomunicaciones y Soluciones Celulares S.A. DE C.V. (en lo sucesivo 'ADDCEL') está de acuerdo en prestar y 'EL CLIENTE' en hacer uso de 'EL SERVICIO' para efectuar operaciones comerciales a través del teléfono celular (en adelante 'EL SERVICIO'), conforme a las cláusulas de este Contrato.  Para tener acceso a 'EL SERVICIO', 'EL CLIENTE' deberá contar con un teléfono celular que esté habilitado técnicamente para operar 'EL SERVICIO'. <br/> <br/>SEGUNDA. CONEXION.- 'ADDCEL' autoriza a 'EL CLIENTE' para que a través del teléfono celular y previa autenticación mediante la línea telefónica de su teléfono celular y su contraseña, se pueda conectar a 'EL SERVICIO', y de esta manera pueda tener acceso a los equipos y sistemas de cómputo electrónico (en adelante el 'Computador Central' o 'SISTEMA') que 'ADDCEL' tiene en funcionamiento. <br/> <br/>TERCERA.- Lograda la conexión a que alude la Cláusula Segunda, 'EL CLIENTE' podrá tener acceso a las cuentas bancarias que 'EL CLIENTE' designe, en lo sucesivo 'LAS CUENTAS' para efectuar las siguientes operaciones: <br/>a.	Consulta de Estado de cuenta <br/>b.	Compra de los productos afiliados a la app. <br/>c.     Transferencias a cuentas de terceros. <br/>d.     Cambio de contraseña del servicio, así como toda la información proporcionada por 'EL CLIENTE' como su nombre, dirección, tarjeta, contraseña, etc. <br/>e.     Pago de Servicios afiliados. <br/>f.      Cualquier otra operación y/o servicio que 'ADDCEL'  llegare a autorizar en el futuro. <br/>g.     Pago de Impuestos afiliados. <br/> <br/>CUARTA.- Queda expresamente establecido que las operaciones que se llevan a cabo al amparo de 'EL SERVICIO' se realizarán, conforme a lo siguiente: <br/>a. 'EL CLIENTE' únicamente, podrá tener asociado un celular con su respectivo número de línea celular, a 'EL SERVICIO'. <br/>b. Las operaciones podrán realizarse si 'EL CLIENTE' tiene saldo suficiente en 'LA CUENTA' en que se vaya a efectuar el cargo correspondiente. <br/>c. Tratándose de consulta de saldos y movimientos, la información que 'ADDCEL' proporcione a 'EL CLIENTE', corresponderá a la que en sus registros contables aparezca registrada a esa fecha. <br/>d. La información e instrucciones que 'EL CLIENTE' transmita o comunique a 'ADDCEL' al efectuar sus operaciones, así como los comprobantes emitidos y transmitidos por 'ADDCEL', tendrán pleno valor probatorio y fuerza legal para acreditar la operación realizada, el importe de la misma, su naturaleza, así como las características y alcance de sus instrucciones. <br/>e. 'ADDCEL' podrá fijar libremente las bases, requisitos y condiciones de operación de 'EL SERVICIO', así como el límite de las transferencias o aportaciones, teniendo como máximo, aquellas permitidas por la Comisión Nacional Bancaria y de Valores, f. 'EL CLIENTE' podrá establecer los montos para realizar operaciones del servicio y podrá modificar los mismos, sin superar los definidos por la regulación vigente de la Comisión Nacional Bancaria y de PROSA <br/> <br/>QUINTA.- 'ADDCEL' registrará a 'EL CLIENTE' en 'EL SERVICIO' después de autentificarle por medio de los elementos que considere convenientes, como son, enunciativa, más no limitativamente, el número de cuenta y/o el número de plástico asociado a 'LAS CUENTAS', el número de su teléfono celular, fecha de vencimiento, número de cuenta asociada, código de seguridad o preguntas aleatorias de información registrada en los sistemas de 'ADDCEL'. Ambas partes convienen en que el número telefónico que 'EL CLIENTE' dio de alta y su contraseña, sirven de identificación en el sistema y son utilizados en lugar del nombre y firma de 'EL CLIENTE', 'ADDCEL' se basará en ellos de la misma manera y para los mismos propósitos y alcances. <br/> <br/>SEXTA.- MECANICA OPERATIVA. 'ADDCEL ' y 'EL CLIENTE' convienen que 'EL CLIENTE' podrá contratar 'El SERVICIO', conforme a la siguiente mecánica operativa: <br/>1.	Al momento de ingresar al 'SISTEMA', 'EL CLIENTE' deberá llenar el campo solicitado proporcionando el número de su TELEFONO CELULAR, NOMBRE, DIRECCION, TARJETA, CONTRASEñA, ETC. que serán <br/>2.	EL CLIENTE proporcionará los datos que el sistema le solicite para autenticarle como el titular de 'LAS CUENTAS'. <br/>3.	Para ingresar al 'SISTEMA',  'EL CLIENTE' deberá definir una clave numérica (en lo sucesivo 'CLAVE DE OPERACION/ACCESO'). <br/>4.    'EL CLIENTE' deberá confirmar que conoce y acepta los términos y condiciones del servicio mismos que están contenidos en el presente Contrato y que le fueron puestos disponibles en http://www.mobilecard.mx/terminos.html <br/>5.    En el proceso de contratación de 'EL SERVICIO' 'EL CLIENTE' proporcionará la información que le sea solicitada por el sistema una vez verificada la información por 'ADDCEL', se tendrá por contratado 'EL SERVICIO'. <br/>6.    Para la operación de 'EL SERVICIO', 'ADDCEL' validará a 'EL CLIENTE' en el 'SISTEMA',  cuando éste último acécese al sistema y proporcione su número de usuario, más la 'CLAVE DE OPERACION/ACCESO. <br/>7.    'EL CLIENTE' invariablemente para realizar cualquiera de las operaciones estipuladas en este contrato, deberá proporcionar su contraseña y CVV' de la tarjeta registrada. <br/>8.    'EL CLIENTE' reconoce y acepta el carácter personal y confidencial de su 'CLAVE DE OPERACION/ACCESO' (en lo sucesivo la 'CONTRASEñA'). <br/> <br/>SEPTIMA.- CONDICIONES PARA LA PRESTACION DEL SERVICIO.- 'ADDCEL' prestará los servicios, materia de este contrato, siempre que le sean solicitados por el medio previsto, en los días y horas que el propio 'ADDCEL' establezca al efecto. <br/>'ADDCEL' no estará obligado a prestar 'EL SERVICIO', en los siguientes casos: <br/>a. Cuando la información transmitida sea insuficiente, inexacta, errónea, incompleta, etc. <br/>b. Cuando 'LAS CUENTAS' no se encuentren asociadas o registradas en 'EL SERVICIO', o bien se encuentren canceladas aun cuando no hubieren sido dadas de baja en 'EL SERVICIO'. <br/>c. Cuando no se pudieren efectuar los cargos debido a que en 'LAS CUENTAS' no se mantengan fondos disponibles suficientes o bien cuando 'LAS CUENTAS' no tengan saldo a su favor. <br/>d. Por causa de caso fortuito o fuerza mayor, o por cualquier causa ajena al control de 'ADDCEL'. <br/> <br/>OCTAVA. CONFIDENCIALIDAD.- 'EL CLIENTE' para todos los efectos legales a que haya lugar, expresamente reconoce y acepta el carácter personal e intransferible de la 'CONTRASEñA', así como su confidencialidad. <br/>En caso de que 'EL CLIENTE' tenga conocimiento o crea que ha habido cualquier violación de la seguridad tal como el robo o el uso no autorizado de su contraseña o de su teléfono celular vinculado al servicio, deberá de notificarlo inmediatamente a ADDCEL, para bloquear el acceso con ese dispositivo o contraseña. <br/> <br/>NOVENA. PAGO DE BIENES O SERVICIOS A TRAVES DE TELEFONO MOVIL- Para el caso de que 'ADDCEL' ofrezca a 'EL CLIENTE' un sistema de pagos para la compra de bienes y servicios a través de su teléfono celular, 'EL CLIENTE' reconoce y acepta que su contraseña equivale a su firma electrónica y es el medio que lo identifica al realizar disposiciones del saldo en 'LA CUENTA' mediante el uso de su equipo telefónico celular. <br/> <br/>DECIMA. COMUNICADOS Y ENVIO DE INFORMACION- 'EL CLIENTE' autoriza a 'ADDCEL' para que le envíe cualquier tipo de información, así como la notificación de las operaciones de 'LAS CUENTAS' asociadas al servicio, ya sean estas pasiva, activas o servicios bancarios, al proveedor de 'EL CLIENTE' que le presta servicios de telefonía celular o cualquier otro servicio electrónico, para que dicho proveedor le envíe comunicados a su número de teléfono celular, correo electrónico o cualquier otro medio electrónico autorizado por ADDCEL. <br/> <br/>DECIMO PRIMERA. TIPO DE INFORMACION.- 'ADDCEL' a través de éste servicio podrá enviar a 'EL CLIENTE' información no relacionada con 'LAS CUENTAS'. 'ADDCEL ', determinará el contenido y alcance de esta información, en el entendido que el uso por 'EL CLIENTE' de la misma, no implicará responsabilidad alguna para ' ADDCEL '. <br/>'ADDCEL' comunica a 'EL CLIENTE', quien está de acuerdo, que los mensajes son enviados en entornos de comunicación propios de la compañía telefónica con quien tiene suscrito su servicio de telefonía celular, por lo que la integridad de la información una vez enviada por 'ADDCEL', se apega a los lineamientos de dicha telefónica. 'EL CLIENTE' conoce y asume las condiciones de seguridad en las que 'ADDCEL' ofrece el servicio. <br/> <br/>DECIMO SEGUNDA. MEDIDAS DE SEGURIDAD.- En ningún caso ' ADDCEL ' será responsable de afectación alguna, incluyendo, sin límite, daños, pérdidas, gastos directos, indirectos, inherentes o consecuentes que surjan en relación con 'EL SERVICIO' o su uso o imposibilidad de uso por alguna de 'LAS PARTES', o en relación con cualquier falla en el rendimiento, error, omisión, interrupción, defecto, demora en la operación o transmisión, falla de sistema o línea. <br/> <br/>DECIMO TERCERA.- SUSTITUCION DE LA FIRMA AUTOGRAFA. En virtud a lo estipulado en la cláusula anterior, ' ADDCEL ' y 'EL CLIENTE' convienen en términos de lo establecido en las legislaciones aplicables, que el uso de los medios de identificación electrónicos previstos en este instrumento y las operaciones realizadas mediante la transmisión de datos' a través del empleo de las claves y contraseñas en el 'SISTEMA', sustituirán la firma autógrafa, tendrán pleno valor probatorio y fuerza legal para acreditar la firma del presente contrato, ambas partes convienen en que el uso de las claves y contraseñas sirven de medios de autenticación, identificación y expresión del consentimiento de 'EL CLIENTE' en el 'SISTEMA' y que éstas serán utilizadas en sustitución del nombre y la firma autógrafa de 'EL CLIENTE'. ' ADDCEL' se basará en ellos de la misma manera y para los mismos propósitos y alcances. <br/>Asimismo 'EL CLIENTE' en este acto manifiesta su conformidad obligándose en lo sucesivo a reconocer, considerar y/o aceptar como su firma autógrafa los medios de identificación a que se refiere la cláusula segunda, o en su caso los que los sustituyan. <br/>En ningún caso 'ADDCEL' será responsable de algún daño, incluyendo, sin límite, daños, pérdidas, gastos directos, indirectos, inherentes o consecuentes que surjan en relación con el uso de 'EL SERVICIO' o con la imposibilidad de su uso por alguna de las partes, o en relación con cualquier falla en el rendimiento, error, omisión, interrupción, defecto, demora en la operación o transmisión, o falla de sistema o línea. <br/> <br/>DECIMO CUARTA.- MODIFICACIONES A LOS MEDIOS ELECTRONICOS. ' ADDCEL ' podrá en todo momento mejorar la calidad de sus servicios estableciendo modificaciones a las reglas del 'SISTEMA' y/o a los procedimientos de acceso e identificación, con previo aviso a 'EL CLIENTE' ya sea por escrito, mediante el 'SISTEMA' o por cualquier otro medio, con 7 (SIETE) días hábiles de anticipación a la fecha de entrada en vigor de las mismas, de quien se entenderá su aceptación mediante la utilización que haga de dicho 'SISTEMA' después de que éstas hayan entrado en vigor. <br/> <br/>DECIMO QUINTA. MODIFICACIONES.- 'ADDCEL' se reserva el derecho de efectuar modificaciones a las cláusulas de este contrato, Se entenderá que 'EL CLIENTE' otorga su consentimiento a dichas modificaciones si no da aviso de terminación del presente contrato antes de que venza el plazo de 60 (sesenta) días naturales, o bien si transcurrido este plazo continúa realizando operaciones. <br/> <br/>DECIMO SEXTA. PROCEDIMIENTO DE ACLARACIONES.- En caso de que 'EL CLIENTE' tenga alguna aclaración o queja respecto los movimientos u operaciones realizadas a través de 'EL SERVICIO', podrá presentar su aclaración o queja por escrito ante ADDCEL en los siguientes casos: <br/>a) Cuando el Cliente no esté de acuerdo con alguno de los movimientos que aparezcan en los medios electrónicos, ópticos o de cualquier otra tecnología que se hubieren pactado, podrá presentar una solicitud de aclaración dentro del plazo de sesenta días naturales contados a partir de la fecha de la prestación total del servicio. <br/>b)  Una vez recibida la solicitud de aclaración, 'ADDCEL' tendrá un plazo máximo de cuarenta y cinco días para entregar a 'EL CLIENTE' el dictamen correspondiente, salvo cuando la reclamación sea a operaciones realizadas en el extranjero, en este caso el plazo será hasta de ciento ochenta días naturales. <br/>c) Sin perjuicio de lo anterior 'EL CLIENTE podrá solicitar a 'ADDCEL' los registros de las transacciones, para lo cual ADDCEL entregará a 'EL CLIENTE' dicha información por los canales antes señalados, en un plazo de 10 días hábiles, siempre y cuando se trate de operaciones realizadas en las propias cuentas de 'EL CLIENTE' durante los ciento ochenta días naturales previos al requerimiento. <br/> <br/>DECIMO SEPTIMA. VIGENCIA.- El presente Contrato tendrá una duración indefinida. Sin embargo, podrá darse por terminado por cualquiera de 'LAS PARTES', previo aviso dado por escrito a la otra con 30 (treinta) días naturales de anticipación. <br/>No obstante la terminación de 'EL SERVICIO', el mismo seguirá produciendo todos sus efectos legales entre 'LAS PARTES', hasta que 'EL CLIENTE' y 'ADDCEL' hayan cumplido con todas y cada una de sus obligaciones contraídas al amparo del mismo. <br/> <br/>DECIMO OCTAVA. TERMINACION.- 'ADDCEL' podrá dar por terminado, mediante aviso por escrito, el presente contrato, sin responsabilidad en los siguientes casos de incumplimiento por parte de 'EL CLIENTE': <br/>a. Si 'EL CLIENTE' da por terminadas 'LAS CUENTAS'. <br/>b. Si incumple con las obligaciones que contrae sobre el manejo y operación de 'EL SERVICIO' <br/>c. Si faltare al cumplimiento de cualquiera de sus obligaciones bajo estos contratos. <br/>'EL CLIENTE' podrá suspender el servicio a través de los medios que 'ADDCEL' ponga a su disposición. <br/> <br/>DECIMO NOVENA.- JURISDICCION.- Para la interpretación, ejecución y cumplimiento del presente contrato, las partes convienen en sujetarse a la jurisdicción de los Tribunales competentes en el Distrito Federal renunciando expresamente a cualquier fuero que por razón de sus domicilios presentes o futuros pudiera corresponderles. El presente contrato y las referencias consignadas en el mismo, constituyen la expresión de la voluntad de las partes. <br/> <br/>Enteradas las partes del contenido y alcances del presente contrato, 'EL CLIENTE' manifiesta su consentimiento por medios electrónicos a través de la operativa descrita en este contrato y declara que sus datos e información suministrados son verdaderos y se obligada en los términos del presente contrato. <br/> <br/>ANEXO 'A' <br/> <br/>DESCRIPCION DE SERVICIOS PROPORCIONADOS POR ADDCEL A TRAVES DE LA APLICACION MOBILECARD <br/> <br/>I.- Descripcion General <br/> <br/>MobileCard es una aplicacion multicarrier y multiplataforma para dispositivos movilmoviles que permite el acceso a un catalogo de productos y servicios que ADDCEL ofrecera a sus usuarios por medio de dicha aplicacion. El catalogo se encuentra alojado en una aplicacion Web que permite a ADDCEL dar mantenimiento a cada uno de los servicios y productos que ofrezca y que estaran sujetos a los terminos y condiciones de los servicios que ‘LAS PARTES’  expresamente acordaron en el Contrato de Prestacion de Servicios o pudiesen acordar. Esto permitira que cada usuario de la aplicacion tenga acceso a tantos servicios y productos tenga dados de alta y que pueda realizar compras relacionadas con estos. La transaccionalidad relacionada a estas compras es provista por servicios web externos a la aplicacion. <br/>La aplicacion movil esta desarrollada para cuatro plataformas distintas, las cuales son: <br/>a)            Android <br/>b)           BlackBerry <br/>c)            J2EM (Java) <br/>d)           iOS (iPhone, iPod touch, iPad) <br/> <br/>Asimismo las descargas se haran por medio de: <br/>a.            Pagina de descarga de la aplicacion (http://mobilecard.addcelapp.com) <br/>b.            App Store (iOS) <br/>c.            App World (Blackberry) <br/>d.            OVI Store <br/>e.            Android Market <br/>f.             Vía campañas de SMS <br/> <br/>II.- Interface de Usuario <br/> <br/>Login de usuario: Cada vez que el usuario abre la aplicacion se le solicita ingresar el usuario y contraseña que registro cuando se dio de alta en el sistema. La pantalla le solicita al usuario los siguientes campos: <br/>a)            Usuario <br/>b)           Contraseña <br/> <br/>Los datos son encriptados y enviados al servicio que atiende el modulo del lado del servidor. Este responde con el resultado de la operacion de una manera igualmente encriptada. <br/> <br/>III.- Registro de Usuario <br/> <br/>El usuario debe registrarse en el sistema de MobileCard para realizar las compras de los productos y servicios asi como de acceder a la funcionalidad de la aplicacion tal como es el consultar sus movimientos. Los campos que se le solicitan al usuario al momento de llevar a cabo el registro son: <br/>a)            Usuario <br/>b)           Contraseña <br/>c)            Nombre <br/>d)           Fecha de Nacimiento <br/>e)           DireccionDireccion <br/>f)            No. De Celular <br/>g)            No. De Tarjeta de Crédito <br/>h)           Banco <br/>i)             Tipo de Tarjeta <br/>j)             Fecha de Vencimiento <br/> <br/>Todos los campos son enviados al servidor para su almacenamiento en las bases de datos de la aplicacion. <br/> <br/>IV.- Mi Cuenta <br/> <br/>El usuario una vez registrado puede cambiar sus datos personales, usuario, contraseña y de tarjeta por medio del módulo ‘Mi Cuenta’. <br/> <br/>V.- Consulta de Operaciones <br/> <br/>El usuario puede llevar a cabo la consulta de las operaciones realizadas a traves de la aplicacion MobileCard en los siguientes periodos: <br/>a)            Hoy <br/>b)           Esta Semana <br/>c)            Este Mes <br/>d)           Mes Anterior <br/> <br/>El servicio muestra la informacion de cada transaccion con los datos de: <br/>a)            Fecha <br/>b)           Concepto <br/>c)            Cargo <br/> <br/>VI.- Compra de Productos <br/> <br/>Cuando el usuario ingrese a la seccion de compras, se invoca un servicio para obtener el listado de productos disponibles. Al seleccionar el tipo de producto, se invoca un nuevo servicio con el cual se obtienen los datos correspondientes a todos los subproductos otorgados por el producto seleccionado detallando el costo de cada uno de estos de manera que el usuario puede elegir lo que desee comprar. <br/>Al seleccionar alguno de los producto listados, se le presenta una pantalla de confirmacion con el detalle del producto a comprar y en la que se desplegara la siguiente leyenda ‘En este acto, autorizo a Radiocomunicaciones y Soluciones Celulares, S.A. de C.V. que lleve a cabo el cargo de las cantidades que resulten por concepto de las operaciones comerciales que lleve a cabo a traves de su multicarrier y multiplataforma para dispositivos movilmoviles que permite el acceso a un catalogo de productos y servicios, como  instrumento de pago mediante el cargo automatico a la tarjeta que di de alta en su sistema.  Esta autorizacion se encontrara vigente desde el momento en que ingrese mi firma electronica confidencial compuesta de una clave, la cual aprueba la operacion.’, si la respuesta es afirmativa, es entonces necesario ingresar la contraseña asociada al usuario, el Numero de CVV2 y la fecha de expiracion de la tarjeta para asegurar la legitimidad de la compra. <br/>Lo anterior se representa en el siguiente flujo: <br/>a.            El usuario descarga la aplicacion; <br/>b.            El usuario se registra; <br/>c.            Ingresa con Usuario y Contraseña; <br/>d.            Ingresa a ‘Mi Tienda’; <br/>e.            Selecciona la tienda donde comprara el producto (T.A., IAVE, Dominos.; etc.) <br/>f.             Selecciona el producto a comprar (Telcel, Iusacell, IAVE Prepago, etc.); <br/>g.            Selecciona el subproducto a comprar (IAVE Prepago 100 pesos, IAVE Prepago 200 pesos, etc.). <br/>h.            Confirma la compra ingresando Contrasena, CVV2, Fecha de expiracion de Tarjeta y Numero de Tarjeta IAVE; <br/>i.             Realiza el pago: <br/>i.             Tarjeta Aceptada: Recibe Folio con numero de confirmacion de la compra; <br/>ii.            Tarjeta Rechazada: Recibe mensaje de Rechazo con el motivo. <br/> <br/>VII.- Promociones <br/> <br/>Al momento de dar clic a la imagen de la promocion, esta enviara al usuario a la seccion de compras.</p>
		</div><!-- /content -->
		<div data-role="footer" data-position="fixed" data-tap-toggle="false" style="background-color:#c60c30;color:#ffffff;cursor:pointer"  onclick="$.mobile.changePage('#adicionales2')">
			<h4>REGRESAR</h4>
		</div>
	</form>
</div><!-- /page two -->

<div data-role="page" id="dialogo">
	<div data-role="popup" id="dialog" data-dismissible="false"  style="width:100%;background-color:#c60c30;color:#ffffff;text-align:center">
		<form>
			<div style="padding:10px 20px;">
			   <p id="dialogT"></p>
			   <p id="dialogMsg"></p>
			   <a href="" id="regresaDialogo" class="ui-shadow ui-btn ui-corner-all" style="background-color:#ffffff;color:#c60c30">CONTINUAR</a>
			</div>
		</form>
	</div>
</div><!-- /page two -->
<div data-role="page" id="dialogoConfirmacion">
	<div data-role="popup" id="dialogConfirm" data-dismissible="false"  style="width:100%;background-color:#c60c30;color:#ffffff;text-align:center">
		<form>
			<div style="padding:10px 20px;">
			   <p id="dialogTC"></p>
			   <p id="dialogMsgC"></p>
			   <div class="ui-grid-a">
				<div class="ui-block-a">
			   		<a href="" id="regresaDialogoConf" class="ui-shadow ui-btn ui-corner-all" style="background-color:#ffffff;color:#c60c30">CANCELAR</a>
				</div>
				<div class="ui-block-b">
			   		<a href="" id="continuaDialogoConf" class="ui-shadow ui-btn ui-corner-all" style="background-color:#ffffff;color:#c60c30">CONTINUAR</a>
				</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div id="procom3DSecure" data-role="page">
	<form id="datos3dSecure" name="datos3dSecure" >
            <input type="hidden" name="total">
            <input type="hidden" name="currency">
            <input type="hidden" name="address">
            <input type="hidden" name="order_id">
            <input type="hidden" name="merchant">
            <input type="hidden" name="store">
            <input type="hidden" name="term">
            <input type="hidden" name="digest">
            <input type="hidden" name="return_target">
            <input type="hidden" name="usuario">
            <input type="hidden" name="urlBack">
            <input type="hidden" name="referencia">
	</form>
	<div data-role="content" id="procom3DSecureContent">
	</div>
</div>
<div id="block-ui" style="display:none">
	<div id="sprite">
			<div class="slide">
				<div class="img img1"> </div>
				<div class="img img2"> </div>
				<div class="img img3"> </div>
				<div class="img img4"> </div>
				<div class="img img5"> </div>
				<div class="img img6"> </div>
				<div class="img img7"> </div>
				<div class="img img8"> </div>
				<div class="img img9"> </div>
				<div class="img img10"> </div>
				<div class="img img11"> </div>
				<div class="img img12"> </div>
				<div class="img img13"> </div>
				<div class="img img14"> </div>
				<div class="img img15"> </div>
				<div class="img img16"> </div>
			</div>
		</div>
	<span id="blockMsg"></span>
</div>
<input type="hidden" name="flujoAdicionales" id="flujoAdicionales"/>
<input type="hidden" name="flujoLogin" id="flujoLogin" value="L"/>
<input type="hidden" name="paginaActual" id="paginaActual" value=""/>
</body>
</html>