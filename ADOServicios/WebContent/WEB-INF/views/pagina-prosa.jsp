<%@page import="java.io.PrintWriter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">-->
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
<%--
#java/jsp/html
################################################################################
# Nombre del Programa :prosa_comercio_validaciones.jsp                         #
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :N/A                                   Fecha: N/A        #
# Descripcion General :Pagina para comercio electronico                        # 
# Programa Dependiente:N/A                                                     #
# Programa Subsecuente:N/A                                                     #
# Cond. de ejecucion  :N/A                                                     #
# Dias de ejecucion   :N/A                                      Horario:N/A    #
#                              MODIFICACIONES                                  #
#------------------------------------------------------------------------------#
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :C-04-2761-10                             Fecha:14/10/10 #
# Modificacion        :Nivelacion de Procom                                    #
# Marca de cambio     :C-04-2761-10 Acriter NAC                                #
#------------------------------------------------------------------------------#
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :C-04-2761-10 Fase2                       Fecha:20/01/11 #
# Modificacion        :Nivelacion de Procom Fase2                              #
# Marca de cambio     :Acriter NAC C-04-2761-10 Fase2                          #
#------------------------------------------------------------------------------#
# Numero de Parametros:N/A                                                     #
# Parametros Entrada  :N/A                                      Formato:N/A    #
# Parametros Salida   :N/A                                      Formato:N/A    #
################################################################################
--%>
<head>
    <title>Purchase Verification</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="HandheldFriendly" content="true"/>   
</head>
<%@page import="java.util.Enumeration"%>
<%//@page import="com.acriter.abi.procom.utils.StringHelper"%>
<%//@page import="com.acriter.abi.procom.model.constants.RequestParam"%>
<body>

<% 
String host = request.getParameter("host");
String sessionid = request.getParameter("sessionid");
Enumeration en = request.getParameterNames();


if (host != null && !host.equals("null") && !host.equals("")  && sessionid != null && !sessionid.equals("null") && !sessionid.equals("")) { %>
<link rel=stylesheet href="http://<%= host %>/clear.png?session=<%= sessionid %>">
<object type="application/x-shockwave-flash" data="https://<%= host %>/fp.swf" width="1" height="1" id="thm_fp"><param name="movie" value="https://<%= host %>/fp.swf"/><param name="FlashVars" value="session=<%= sessionid %>" /></object>
<script src="https://<%= host %>/check.js?session=<%= sessionid %>" type="text/javascript"></script>
<% } %>

<div id="contenedor">
		<p style="text-align: center;">Portal 3D Secure ADO</p>
		<p>Por favor proporcione la siguiente información:
			Información de la tarjeta de Crédito</p>
<%-- Checar cambiar el action por el que esta a continuacion --%>
<%-- Invalidando Session --%>
<%-- session.invalidate(); --%>
<!-- Modificacion: Marca de inicio Acriter NAC C-04-2761-10 Fase2 --> 
<FORM METHOD="POST" AUTOCOMPLETE="OFF" ACTION="regresoPago">
<!--<FORM METHOD="POST" AUTOCOMPLETE="OFF" ACTION="./validaciones/valida.do">-->
<!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->

<input type="hidden" name="data_sent" value="1">

  
<%-- ------------------------------ Variables de Mas para el nuevo Procom ------------------------------- --%>
<input type="hidden" name="returnContext" value="<%=request.getContextPath()%>"/>
<input type="hidden" name="urlMerchant" value="<%=request.getServletPath()%>"/>
<!-- Modificacion: Marca de inicio Acriter NAC C-04-2761-10 Fase2 -->
<input type="hidden" name="urlpost" value="/urlpost.jsp"/>
<input type="hidden" name="urlerror" value="/urlpost.jsp"/>
<!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->
<input type="hidden" name="acquirer" value="83">
<input type="hidden" name="source" value="100">


<%
	
		String name = null;
		String value = null;

		while(en.hasMoreElements()){
		  name = (String)en.nextElement();
		  value = request.getParameter(name);
		  
		  %>
		  <input type="hidden" name="<%=name%>" value="<%=value%>">
		  <%
				
		}            				
 %>
 <!-- Datos de prueba simulando respuesta de prosa(borrar) -->
 <input type="hidden" name="EM_Response" value="approved"/>
 <input type="hidden" name="EM_Total" value="<%=request.getParameter("total")%>"/>
 <input type="hidden" name="EM_OrderID" value="<%=request.getParameter("order_id")%>"/>
 <input type="hidden" name="EM_Merchant" value="7454431"/>
 <input type="hidden" name="EM_Store" value="1234"/>
 <input type="hidden" name="EM_Term" value="001"/>
 <input type="hidden" name="EM_RefNum" value="000000000035"/>
 <input type="hidden" name="EM_Auth" value="057504"/>
 <input type="hidden" name="EM_Digest" value="d7c9e2cfaae47c6d7ffe0bad331971cca77932dc"/>
<%-- --------------------------------------------------------------------- --%>

    <table style="width:100%">
				<tbody>
					<tr>
						<td colspan="2">Nombre:</td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="cc_name" size="40,1" maxlength="40" value="" required="true" /></td>
					</tr>
					<tr>
						<td colspan="2">Número de Tarjeta:</td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="cc_number" size="40,1"	maxlength="19" value="" required="true" /></td>
					</tr>
					<tr>
						<td colspan="2">Tipo:</td>
					</tr>
					<tr>
						<td colspan="2">
							<select name="cc_type">
									<option value="Visa">VISA</option>
									<option value="Mastercard">MasterCard</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">Fecha de Vencimiento (mes-año):</td>
					</tr>
					<tr>
						<td ><select  name="_cc_expmonth">
								<option value="01">1</option>
								<option value="02">2</option>
								<option value="03">3</option>
								<option value="04">4</option>
								<option value="05">5</option>
								<option value="06">6</option>
								<option value="07">7</option>
								<option value="08">8</option>
								<option value="09">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
						</select> </td><td><select name="_cc_expyear">
								<%
									java.util.Calendar C = java.util.Calendar.getInstance();
									int anio = C.get(java.util.Calendar.YEAR);
									for (int i = 0; i < 15; i++) {
										out.println("<option value=\""+anio+"\">" + anio+ "</option>");
										anio++;
									}
								%>
						</select></td>
					</tr>
					<tr>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td colspan="2">Código de seguridad(CVV2/CVC2):</td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="cc_cvv2" size="3,1" maxlength="3" value="" required="true" /></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Pagar" /></td>
					</tr>
				</tbody>
			</table>
</form>
</div>
</body>
</html>
