//var urlB="http://50.57.192.214:8080/ADOServicios/";
var urlB="";
var password_ ="";


$(document).bind("mobileinit", function(){ 
    $.mobile.pushStateEnabled = false; 
}); 

jQuery(document).ready(function(){
	$.validator.addMethod(
	        "regex",
	        function(value, element, regexp) {
	            if (regexp.constructor != RegExp)
	                regexp = new RegExp(regexp);
	            else if (regexp.global)
	                regexp.lastIndex = 0;
	            return this.optional(element) || regexp.test(value);
	        },
	        "Please check your input."
	);

	$.validator.addMethod("maxDate", function(value, element) {
		var curDate = new Date();
	    var dateFormat=value.substring(0,2)+'/01/'+value.substring(3);
	    var inputDate = new Date(dateFormat);
	    if ((inputDate.getMonth() < curDate.getMonth())&&(inputDate.getFullYear()<=curDate.getFullYear()))
			return false;
		return true;
	}, "Invalid Date!");
	
	$("[id=goBackButton]").click(function(){goBack()});
	
	$('#adicionales1Continua').click(function(){
		var form = $( "#frmAdicionales1" );
		if(form.valid()){
			if($("#flujoAdicionales").val()=='R'){
				muestraConfirmacion('#adicionales1','#adicionales2','CONFIRMACIÓN','EL NÚMERO CELULAR Y EL EMAIL QUE NOS PROPORCIONO SON CORRECTOS ?, CON ESTA INFORMACIÓN LE SERA ENVIADO UN SMS O UN EMAIL CON LOS DATOS DE ACCESO A SU CUENTA');
			}else if($("#flujoAdicionales").val()=='U'){
				muestraConfirmacion('#adicionales1','#adicionales2','CONFIRMACIÓN','EL EMAIL QUE NOS PROPORCIONO ES CORRECTO ?, A ESTA CUENTA LE SERA ENVIADO UN EMAIL CON LOS DATOS DE ACCESO A SU CUENTA');
			}
			}
		return false;
	});

	$('#adicionales2Continua').click(function(){
		var form = $( "#frmAdicionales2" );
		if(form.valid()){	
			if($("#flujoAdicionales").val()=='R'){
				datosUsuario("registro");
			}else if($("#flujoAdicionales").val()=='U'){
				datosUsuario("actualiza");
			}
		}
		return false;
	});

	$('#btnRegistro').click(function(){
		document.frmAdicionales1.login.value=loginSugerido;
		document.frmAdicionales1.email.value=email;
		document.frmAdicionales1.confemail.value=email;
		$("#flujoAdicionales").val("R");
		$.mobile.changePage('#adicionales1');
		return false;
	});

	$('#btnDesligarCuentas').click(function(){
		$("#flujoLogin").val("D");
		$.mobile.changePage('#pagelogin');
		return false;
	});
	
	$('#btnActualizarRegistro').click(function(){
		$("#flujoLogin").val("A");
		$("#flujoAdicionales").val("U");
		$.mobile.changePage('#pagelogin');
		return false;
	});

	$('#botonLogin').click(function(){
		if($("#flujoLogin").val()==='L'){
			loginMC('loginMC','#pagelogin');
		}else if($("#flujoLogin").val()==='A'){
			loginMC('actualizaInicio','#editaDatos');
		}
		else if($("#flujoLogin").val()==='D'){
			loginMC('desligarCuenta','#editaDatos');
		}
	});
	
	$("#plataforma").val(navigator.platform);
	

});

$(document).on("pageshow", "#modificarPass", function() {
	$('#frmModificaPass').validate({
		rules: {
			login: {
				required: true
			},
			oldPassword: {
				required: true,
				minlength: 8,
				maxlength: 12
			},
			newPassword: {
				required: true,
				minlength: 8,
				maxlength: 12
			},
			confPassword: {
				required: true,
				minlength: 8,
				maxlength: 12,
				equalTo: "#newPassword"
			}
		},
		messages: {
			login: {
			  required: 'EL LOGIN ES REQUERIDO'
			},
			oldPassword: {
			  required: 'EL PASSWORD ANTERIOR ES REQUERIDO',
			  minlength: 'LA LONGITUD DEL PASSWORD DEBE SER MAYOR A 8',
			  maxlength: 'LA LONGITUD DEL PASSWORD DEBE SER MENOR A 12'
			},
			newPassword: {
			  required: 'EL PASSWORD NUEVO ES REQUERIDO',
			  minlength: 'LA LONGITUD DEL PASSWORD DEBE SER MAYOR A 8',
			  maxlength: 'LA LONGITUD DEL PASSWORD DEBE SER MENOR A 12'
			},
			confPassword: {
			  required: 'LA CONFIRMACION ES REQUERIDA',
			  minlength: 'LA LONGITUD DEL PASSWORD DEBE SER MAYOR A 8',
			  maxlength: 'LA LONGITUD DEL PASSWORD DEBE SER MENOR A 12',
			  equalTo: 'EL PASSWORD NO COINCIDE'
			}
		},
		errorPlacement: function(error, element) {
			error.insertAfter(element.parent()); 
		},
		highlight: function(element, errorClass, validClass) {
			$(element).parent().addClass("textFieldError"); 
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parent().removeClass("textFieldError"); 
		}
	});
});

$(document).on("pageshow", "#pagelogin", function() {
	$('#frmPageLogin').validate({
		rules: {
			login: {
				required: true
			},
			password: {
				required: true,
				minlength: 8,
				maxlength: 12
			}
		},
		messages: {
			login: {
			  required: 'EL LOGIN ES REQUERIDO'
			},
			password: {
			  required: 'EL PASSWORD ES REQUERIDO',
			  minlength: 'LA LONGITUD DEL PASSWORD DEBE SER MAYOR A 8',
			  maxlength: 'LA LONGITUD DEL PASSWORD DEBE SER MENOR A 12'
			}
		},
		errorPlacement: function(error, element) {
			error.insertAfter(element.parent()); 
		},
		highlight: function(element, errorClass, validClass) {
			$(element).parent().addClass("textFieldError"); 
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parent().removeClass("textFieldError"); 
		}
	});
});

$(document).on("pageshow", "#recuperaPass", function() {
	$('#frmPageRecupera').validate({
		rules: {
			login: {
				required: true
			}
		},
		messages: {
			login: {
			  required: 'EL LOGIN ES REQUERIDO'
			}
		},
		errorPlacement: function(error, element) {
			error.insertAfter(element.parent()); 
		},
		highlight: function(element, errorClass, validClass) {
			$(element).parent().addClass("textFieldError"); 
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parent().removeClass("textFieldError"); 
		}
	});
});

$(document).on("pageshow", "#adicionales1", function() {
	$('#frmAdicionales1').validate({
		rules: {
			login: {
				required: true
			},
			celular: {
				required: true,
				minlength: 10,
				maxlength: 10,
				digits:true
			},
			confcelular: {
				required: true,
				equalTo: "#celular"
			},
			email: {
				required: true,
				email: true
			},
			confemail: {
				required: true,
				equalTo: "#email"
			},
			nombre: {
				required: true
			},
			apellidoP: {
				required: true
			},
			apellidoM: {
				required: true
			},
			fechaNac: {
				required: true
			},
			telefonoCasa: {
				minlength: 10,
				maxlength: 10,
				digits:true
			},
			telefonoOficina: {
				minlength: 10,
				maxlength: 10,
				digits:true
			}
		},
		messages: {
			login: {
				required: 'EL LOGIN ES REQUERIDO'
			},
			celular: {
				required: 'EL CELULAR ES REQUERIDO',
				minlength: 'EL CELULAR DEBE TENER 10 DIGITOS',
				maxlength: 'EL CELULAR DEBE TENER 10 DIGITOS',
				digits:'EL FORMATO DEL TELEFONO NO ES CORRECTO'
			},
			confcelular: {
				required:'LA CONFIRMACIÓN ES REQUERIDA',
				equalTo: 'EL CELULAR NO ES IGUAL'
			},
			email: {
				required: 'EL EMAIL ES REQUERIDO',
				email: 'EL FORMATO DEL EMAIL NO ES CORRECTO'
			},
			confemail: {
				required: 'LA CONFIRMACIÓN ES REQUERIDA',
				equalTo: 'EL EMAIL NO ES IGUAL'
			},
			nombre: {
				required: 'EL NOMBRE ES REQUERIDO'
			},
			apellidoP: {
				required: 'EL APELLIDO PATERNO ES REQUERIDO'
			},
			apellidoM: {
				required: 'EL APELLIDO MATERNO ES REQUERIDO'
			},
			fechaNac: {
				required: 'LA FECHA DE NACIMIENTO REQUERIDA'
			},
			telefonoCasa: {
				minlength: 'EL TELEFONO DEBE TENER 10 DIGITOS',
				maxlength: 'EL TELEFONO DEBE TENER 10 DIGITOS',
				digits:'EL FORMATO DEL TELEFONO NO ES CORRECTO'
			},
			telefonoOficina: {
				minlength: 'EL TELEFONO DEBE TENER 10 DIGITOS',
				maxlength: 'EL TELEFONO DEBE TENER 10 DIGITOS',
				digits:'EL FORMATO DEL TELEFONO NO ES CORRECTO'
			}
		},
		errorPlacement: function(error, element) {
			error.insertAfter(element.parent()); 
		},
		highlight: function(element, errorClass, validClass) {
			$(element).parent().addClass("textFieldError"); 
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parent().removeClass("textFieldError"); 
		}
	});
	if($("#flujoAdicionales").val()==="R"){
		$("#frmAdicionales1 input[name=login]").attr('readonly',false);
	}else{
		$("#frmAdicionales1 input[name=login]").attr('readonly',true);
	}
});
$(document).on("pageshow", "#adicionales2", function() {
	$('#frmAdicionales2').validate({
		rules: {
			ciudad: {
				required: true
			},
			calle: {
				required: true
			},
			numeroExt: {
				required: true
			},
			codigoPostal: {
				required: true
			},
			numTarjeta: {
				required: true,
				minlength: 16,
				maxlength: 16,
				regex:/^(4[0-9]{12}(?:[0-9]{3})?)|(5[1-5][0-9]{14})$/
			},
			vigenciaTarjeta: {
				required: true,
				maxDate: true
			},
			terminos:{
				range: [1, 1]
			}
		},
		messages: {
			ciudad: {
				required: 'LA CIUDAD ES REQUERIDA'
			},
			calle: {
				required: 'LA CALLE ES REQUERIDA'
			},
			numeroExt: {
				required: 'EL NUMERO EXTERIOR ES REQUERIDO'
			},
			codigoPostal: {
				required: 'LA CODIGO POSTAL ES REQUERIDO'
			},
			numTarjeta: {
				required: 'LA TARJETA ES REQUERIDA',
				minlength: 'LA TARJETA DEBE TENER 16 DIGITOS',
				maxlength:  'LA TARJETA DEBE TENER 16 DIGITOS',
				regex: 'LA TARJETA NO TIENE UN FORMATO VALIDO'
			},
			vigenciaTarjeta: {
				required: 'LA VIGENCIA ES REQUERIDA',
				maxDate: 'LA VIGENCIA NO ES VALIDA',
			},
			terminos:{
				range: 'ES NECESARIO ACEPTAR LOS TERMINOS'
			}
		},
		errorPlacement: function(error, element) {
			if($(element).attr("id")!=="terminos"){
				error.insertAfter($(element).parent());   
			}else{
				error.insertAfter($(element)); 
			} 
		},
		highlight: function(element, errorClass, validClass) {
			if($(element).attr("id")!=="terminos"){
				$(element).parent().addClass("textFieldError"); 
			}else{
				$(element).addClass("textFieldError"); 
			}
		},
		unhighlight: function(element, errorClass, validClass) {
			if($(element).attr("id")!=="terminos"){
				$(element).parent().removeClass("textFieldError");  
			}else{
				$(element).removeClass("textFieldError"); 
			} 
		}
	});
});

$(document).on("pageshow", "#pago", function() {
	$('#frmPago').validate({
		rules: {
			password: {
				required: true,
				minlength: 8,
				maxlength: 12
			}
		},
		messages: {
			password: {
				required: 'EL PASSWORD ES REQUERIDO',
				minlength: 'LA LONGITUD DEL PASSWORD DEBE SER MAYOR A 8',
				maxlength: 'LA LONGITUD DEL PASSWORD DEBE SER MENOR A 12'
			}
		},
		errorPlacement: function(error, element) {
			error.insertAfter($(element).parent()); 
		},
		highlight: function(element, errorClass, validClass) {
			$(element).parent().addClass("textFieldError"); 
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parent().removeClass("textFieldError"); 
		}
		});
});

$(document).on('pageinit', '#adicionales1' ,function() {
	var curr = new Date().getFullYear();
	$("#proveedor").scroller({preset: 'select', 
                    theme: 'android-ics light',
                    mode: 'mixed',
                    lang:'es',
                    display: 'bottom',
                    animate: 'slidedown'
               });
	$("#sexo").scroller({preset: 'select', 
                    theme: 'android-ics light',
                    mode: 'mixed',
                    lang:'es',
                    display: 'bottom',
                    animate: 'slidedown'
               });
	$("#fechaNac").scroller({preset: 'date',
					dateOrder: 'yyMMdd',
                    dateFormat: 'yy-mm-dd',
                    startYear: curr-80,
                    endYear: curr -8,
                    width: 100,
                    theme: 'android-ics light',
                    mode: 'mixed',
                    lang:'es',
                    display: 'bottom',
                    animate: 'slidedown'
               });
	$("#proveedor_dummy" ).textinput();
	$("#sexo_dummy" ).textinput();

	/*$("#proveedor_dummy" ).textinput();
	$("#proveedor_dummy").parent().append('<span onclick="$(\'#proveedor_dummy\').trigger(\'click\');" class="ui-input-clear ui-btn ui-icon-Ado-flecha"></span>');
	$("#proveedor_dummy").parent().removeClass( "ui-input-text" );
	$("#proveedor_dummy").parent().addClass( "ui-input-myicon" );
	$("#proveedor_dummy").parent().addClass("ui-input-has-clear");
	
	$("#fechaNac").parent().append('<span onclick="$(\'#fechaNac\').trigger(\'click\');" class="ui-input-clear ui-btn ui-icon-Ado-flecha"></span>');
	$("#fechaNac").parent().removeClass( "ui-input-text" );
	$("#fechaNac").parent().addClass( "ui-input-myicon" );
	$("#fechaNac").parent().addClass("ui-input-has-clear");
	
	$("#sexo_dummy" ).textinput();
	$("#sexo_dummy").parent().append('<span onclick="$(\'#sexo_dummy\').trigger(\'click\');" class="ui-input-clear ui-btn ui-icon-Ado-flecha"></span>');
	$("#sexo_dummy").parent().removeClass( "ui-input-text" );
	$("#sexo_dummy").parent().addClass( "ui-input-myicon" );
	$("#sexo_dummy").parent().addClass("ui-input-has-clear");*/
	
});

$(document).on('pageinit', '#adicionales2' ,function() {
	var curr = new Date().getFullYear();
	$("#estado").scroller({preset: 'select', 
                    theme: 'android-ics light',
                    mode: 'mixed',
                    lang:'es',
                    display: 'bottom',
                    animate: 'slidedown'
               });
	$("#tipoTarjeta").scroller({preset: 'select', 
                    theme: 'android-ics light',
                    mode: 'mixed',
                    lang:'es',
                    display: 'bottom',
                    animate: 'slidedown'
               });
	$("#vigenciaTarjeta").scroller({preset: 'date',
					dateOrder: 'mmyy',
                    dateFormat: 'mm/yy',
                    startYear: curr,
                    endYear: curr + 10,
                    width: 100,
                    theme: 'android-ics light',
                    mode: 'mixed',
                    lang:'es',
                    display: 'bottom',
                    animate: 'slidedown'
               });
	$("#estado_dummy" ).textinput();
	$("#tipoTarjeta_dummy" ).textinput();
	/*$("#estado_dummy" ).textinput();
	$("#estado_dummy").parent().append('<span onclick="$(\'#estado_dummy\').trigger(\'click\');" class="ui-input-clear ui-btn ui-icon-Ado-flecha"></span>');
	$("#estado_dummy").parent().removeClass( "ui-input-text" ).addClass( "ui-input-myicon ui-input-has-clear");
	
	$("#vigenciaTarjeta").parent().append('<span onclick="$(\'#vigenciaTarjeta\').trigger(\'click\');" class="ui-input-clear ui-btn ui-icon-Ado-flecha"></span>');
	$("#vigenciaTarjeta").parent().removeClass( "ui-input-text" ).addClass( "ui-input-myicon ui-input-has-clear");
	
	$("#tipoTarjeta_dummy" ).textinput();
	$("#tipoTarjeta_dummy").parent().append('<span onclick="$(\'#tipoTarjeta_dummy\').trigger(\'click\');" class="ui-input-clear ui-btn ui-icon-Ado-flecha"></span>');
	$("#tipoTarjeta_dummy").parent().removeClass( "ui-input-text" ).addClass( "ui-input-myicon ui-input-has-clear");*/
	
});

$(document).on('pageinit', '#welcome' ,function() {
	$("#logoW").css("height",$(document).height()/3);
	$("#logoW").css("width",$(document).height()/3);
	$("#logoW").css("margin",new String(margenElemento(40, $(document).height()/3 ,0) + 'px auto'));
	//$("#btnLogin").css("margin",new String(margenElemento(30, 100,0) + 'px auto'));
	//$("#btnRegistro").css("margin",new String(margenElemento(30, 100,0) + 'px auto'));
});

$(document).on('pageinit', '#editaDatos' ,function() {
	$("#logoE").css("height",($(document).height()/5)*2);
	$("#logoE").css("width",($(document).height()/5)*2);
	$("#logoE").css("margin",new String(margenElemento(40, ($(document).height()/5)*2, 0) + 'px auto'));
	//$("#btnDesligarCuentas").css("margin",new String(margenElemento(15, 20,87) + 'px auto'));
	//$("#btnCambiaPass").css("margin",new String(margenElemento(15, 20,87) + 'px auto'));
	//$("#btnResetearPass").css("margin",new String(margenElemento(15, 20,87) + 'px auto'));
	//$("#btnActualizarRegistro").css("margin",new String(margenElemento(15, 20,87) + 'px auto'));
});


function margenElemento(porCont, tamElementoY, menos) {
    var height = $(document).height()-menos;
    var tamPanel = height * (porCont / 100);
    return (tamPanel - tamElementoY) / 2;
}

/*$(document).bind("mobileinit", function(){
	$.mobile.pageLoading(true);
	$.mobile.ajaxLinksEnabled = false;
	$.mobile.loadPage.defaults.showLoadMsg = false;
	$.mobile.changePage.defaults.showLoadMsg = false;
    $.mobile.autoInitialize = false;
  });
	
$(document).ready(function(){
	var idError='${idError}';
	document.location.hash = "${pantalla}";
    if(idError!=="1"){
    	muestraDialogo("#${pantalla}","ERROR",'${mensajeError}');
    }
	$.mobile.initializePage();
  });*/

function bloqueaUi(msg){
	$("#blockMsg").html(msg);
	$.blockUI({ 
		 message:$('#block-ui'),
		 css: {   
			 	width:          '40%',
			 	border:          '0px', 
			 	left:       '30%',
                backgroundColor: '#fff', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: 1                
            }
		}); 
}
	
$(document).ready(function(){
	var pass = '';
	for (var i = 0; i < 20; i++) {
	    var numI = getRandomNum();
	    while (checkPunc(numI)) {
	        numI = getRandomNum();
	    }
	    pass = pass + String.fromCharCode(numI);
	}
	$.mobile.changePage('#'+pantalla3421);
	password_ = new jsSHA(pass, "ASCII").getHash("SHA-512", "HEX");
	$("#blockMsg").html("Encriptando<BR/>el canal");
	$.blockUI({ 
		 message:$('#block-ui'),
		 css: {   
			    width:          '40%', 
			 	border:          '0px', 
			 	left:       '30%',
                backgroundColor: '#fff', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: 1
            },
         onUnblock: function(){if(idError!=="0")muestraDialogo("#"+pantalla3421,"ERROR",mensajeError3421);}
		}); 
	$.jCryption.authenticate(password_, "crypto?callback=?&generateKeyPair_=true", "crypto?callback=?&handshake_=true",
	        function(AESKey) {
				$.unblockUI(); 
	        }, function() {
				muestraDialogo("#"+pantalla3421,"ERROR","NO SE HA PODIDO ENCRIPTAR EL CANAL");
	        
	});

});
function getRandomNum() {
    return (parseInt(Math.random() * 1000) % 94) + 33;
}

function checkPunc(num) {
    if (((num >= 33) && (num <= 47)) || ((num >= 58) && (num <= 64)) || ((num >= 91) && (num <= 96)) || ((num >= 123) && (num <= 126)))
        return true;
    return false;
}

function muestraDialogo(regreso, titulo, mensaje){
	$("#regresaDialogo").attr("href",regreso);
	$("#dialogT").html(titulo);
	$("#dialogMsg").html(mensaje);
	$("#dialog").popup().popup("open");
	$("#dialog-popup").css('max-width', '100%');
	$("#dialog-popup").css('left', '-1');
}

function muestraConfirmacion(regreso,siguiente, titulo, mensaje){
	$("#regresaDialogoConf").attr("href",regreso);
	$("#continuaDialogoConf").attr("href",siguiente);
	$("#dialogTC").html(titulo);
	$("#dialogMsgC").html(mensaje);
	$("#dialogConfirm").popup().popup("open");
	$("#dialogConfirm-popup").css('max-width', '100%');
	$("#dialogConfirm-popup").css('left', '-1');
}
function loginMC(servicio, origen){
	var form = $( "#frmPageLogin" );
	if(form.valid()){
		postData={data:$.jCryption.encrypt(form.serialize(), password_)};
		newAjax(servicio,postData).done(function( data ) {
				  var obj =null;
				  try{
				  	obj = $.parseJSON(data);
				  	muestraDialogo(origen,"--ERROR",obj.errorMensaje);
				  }catch(err){
						var obj = $.parseJSON($.jCryption.decrypt(data, password_));
						if(servicio==='loginMC'){
							if(obj.idError===98){
								muestraDialogo("#modificarPass","ERROR",obj.errorMensaje);
							}
							else if(obj.idError!==0){
								muestraDialogo(origen,"ERROR",obj.errorMensaje);
							}else{
								$("#loginCompra").html(document.frmPageLogin.login.value);
								$("[id=login]").val(document.frmPageLogin.login.value);
								$.mobile.changePage('#pago');		
							}
						}else if(servicio==='desligarCuenta'){
							if(obj.idError!==1){
								muestraDialogo(origen,"ERROR",obj.errorMensaje);
							}else{
								muestraDialogo("#pagelogin","EXITO",obj.errorMensaje);	
							}
						}else if (servicio==='actualizaInicio'){
							if(obj.idError && obj.idError!==0){
								muestraDialogo(origen,"ERROR",obj.errorMensaje);
							}else{
								$("#loginCompra").html(obj.login);
								$("[id=login]").val(obj.login);
								$("#celular").val(obj.telefono);
								$("#confcelular").val(obj.telefono);
								$("#proveedor").val(obj.proveedor);
								$("#email").val(obj.mail);
								$("#confemail").val(obj.mail);
								$("#nombre").val(obj.nombre);
								$("#apellidoP").val(obj.apellido);
								$("#apellidoM").val(obj.materno);
								$("#fechaNac").val(obj.nacimiento);
								$("#sexo").val(obj.sexo);
								$("#telefonoCasa").val();
								$("#telefonoOficina").val();
								$("#estado").val(obj.id_estado);
								$("#ciudad").val(obj.ciudad);
								$("#calle").val(obj.calle);
								$("#numeroExt").val(obj.num_ext);
								$("#numeroInt").val(obj.num_interior);
								$("#codigoPostal").val(obj.cp);
								$("#tipoTarjeta").val(obj.tipotarjeta);
								$("#numTarjeta").val(obj.tarjeta);
								$("#vigenciaTarjeta").val(obj.vigencia);
								$("#terminos").val(0);
								$("#login").prop('readonly', true);
								$.mobile.changePage('#adicionales1');
							}
						}
						$("#flujoLogin").val("L");
						$("#flujoAdicionales").val("U");
						$("[id=password]").val("");
					}
			  }).fail(function(jqXHR, textStatus, errorThrown) 
				        {
				  muestraDialogo(origen,"ERROR",getMsg(jqXHR,errorThrown));
					$("#flujoLogin").val("L");
					document.frmPageLogin.password.value="";
				        });
	}
}

function recuperaPass(){
	var form = $( "#frmPageRecupera" );
	if(form.valid()){	
		postData={data:$.jCryption.encrypt(form.serialize(), password_)};
		newAjax("recuperaPass",postData).done(function( data ) {
				  var obj = null;
				  try{
				  	obj = $.parseJSON(data);
				  }catch(err){}
					if(typeof(obj) !== "undefined" && obj !== null && obj.length>0){
						muestraDialogo("#recuperaPass","ERROR",obj.errorMensaje);
					}else{
						obj = $.parseJSON($.jCryption.decrypt(data, password_));
						if(obj.idError!==0){
							muestraDialogo("#recuperaPass","ERROR",obj.errorMensaje);
						}else{
							muestraDialogo("#pagelogin","EXITO",obj.errorMensaje);
						}
					}
			  }).fail(function(jqXHR, textStatus, errorThrown) 
				        {
				  			muestraDialogo("#recuperaPass","ERROR",getMsg(jqXHR,errorThrown));
				        });
	}
}

function cambiaPass(){
	var form = $( "#frmModificaPass" );
	if(form.valid()){	
		postData={data:$.jCryption.encrypt(form.serialize(), password_)};
		newAjax("modificaPass",postData).done(function( data ) {
				  var obj = null;
				  try{
				  	obj = $.parseJSON(data);
				  }catch(err){}
					if(typeof(obj) !== "undefined" && obj !== null && obj.length>0){
						muestraDialogo("#modificarPass","ERROR",obj.errorMensaje);
					}else{
						var obj = $.parseJSON($.jCryption.decrypt(data, password_));
						if(obj.idError!==1){
							muestraDialogo("#modificarPass","ERROR",obj.errorMensaje);
						}else{
							muestraDialogo("#pago","EXITO",obj.errorMensaje);
						}
						document.frmModificaPass.oldPassword.value="";
						document.frmModificaPass.newPassword.value="";
						document.frmModificaPass.confPassword.value="";
					}
			  }).fail(function(jqXHR, textStatus, errorThrown) 
				        {
				  			muestraDialogo("#modificarPass","ERROR",getMsg(jqXHR,errorThrown));
				        });
	}
}

function datosUsuario(servicio){
	postData={data:$.jCryption.encrypt($("#frmAdicionales1").serialize()+"&"+$("#frmAdicionales2").serialize(), password_)};
	newAjax(servicio,postData).done(function( data ) {
			  var obj = null ;
			  try{
			  	obj = $.parseJSON(data);
			  }catch(err){}
				if(typeof(obj) !== "undefined" && obj !== null && obj.length>0){
					muestraDialogo("#frmAdicionales2","ERROR",obj.errorMensaje);
				}else{
					var obj = $.parseJSON($.jCryption.decrypt(data, password_));
					if(obj.idError!==0){
						muestraDialogo("#frmAdicionales2","ERROR",obj.errorMensaje);
					}else{
						if(servicio==="registro"){	
							$("#loginCompra").html(document.frmAdicionales1.login.value);					
							$("[id=login]").val(document.frmAdicionales1.login.value);
						}
						muestraDialogo("#pago","EXITO",obj.errorMensaje);
					}
				}
		  }).fail(function(jqXHR, textStatus, errorThrown) 
			        {
			  			muestraDialogo("#frmAdicionales2","ERROR",getMsg(jqXHR,errorThrown));
			        });
}

function validaLogin(textField){
	var login = $(textField).val();
	if(login.length>0){	
		postData={data:$.jCryption.encrypt("login="+login, password_)};
		newAjax("validarLogin",postData).done(function( data ) {
				  var obj = null;
				  try{
				  	obj = $.parseJSON(data);
				  }catch(err){}
					if(typeof(obj) !== "undefined" && obj !== null && obj.length>0){
						muestraDialogo("#adicionales1","ERROR",obj.errorMensaje);
					}else{
						obj = $.parseJSON($.jCryption.decrypt(data, password_));
						if(obj.idError!==0){
							muestraDialogo("#adicionales1","ERROR",obj.errorMensaje);
							document.frmAdicionales1.login.value="";
						}
					}
			  }).fail(function(jqXHR, textStatus, errorThrown) 
				        {
				  			muestraDialogo("#adicionales1","ERROR",getMsg(jqXHR,errorThrown));
				        });
	}
}

function validaDN(textField){
	var dn = $(textField).val();
	if(dn.length===10){
		postData={data:$.jCryption.encrypt("dn="+dn, password_)};
		newAjax("validarDN",postData).done(function( data ) {
				  var obj = null;
				  try{
				  	obj = $.parseJSON(data);
				  }catch(err){}
					if(typeof(obj) !== "undefined" && obj !== null && obj.length>0){
						muestraDialogo("#adicionales1","ERROR",obj.errorMensaje);
					}else{
						var obj = $.parseJSON($.jCryption.decrypt(data, password_));
						if(obj.idError!==0){
							muestraDialogo("#adicionales1","ERROR",obj.errorMensaje);
							document.frmAdicionales1.celular.value="";
						}
					}
			  }).fail(function(jqXHR, textStatus, errorThrown) 
				        {
				  			muestraDialogo("#adicionales1","ERROR",getMsg(jqXHR,errorThrown));
				        });
	}
}
function validaEmail(textField){
	var email = $(textField).val();
	if(email.length>0){
		postData={data:$.jCryption.encrypt("email="+email, password_)};
		newAjax("validarEmail",postData).done(function( data ) {
				  var obj = null;
				  try{
				  	obj = $.parseJSON(data);
				  }catch(err){}
					if(typeof(obj) !== "undefined" && obj !== null && obj.length>0){
						muestraDialogo("#adicionales1","ERROR",obj.errorMensaje);
					}else{
						obj = $.parseJSON($.jCryption.decrypt(data, password_));
						if(obj.idError!==0){
							muestraDialogo("#adicionales1","ERROR",obj.errorMensaje);
							document.frmAdicionales1.email.value="";
						}
					}
			  }).fail(function(jqXHR, textStatus, errorThrown){
				  			muestraDialogo("#adicionales1","ERROR",getMsg(jqXHR,errorThrown));
				        });
	}
}

function validaTDC(textField){
	var numTarjeta = $(textField).val();
	if(numTarjeta.length==16){
		postData={data:$.jCryption.encrypt("numTarjeta="+numTarjeta, password_)};
		newAjax("validarTDC",postData).done(function( data ) {
				  var obj = null;
				  try{
				  	obj = $.parseJSON(data);
				  }catch(err){}
					if(typeof(obj) !== "undefined" && obj !== null && obj.length>0){
						muestraDialogo("#adicionales2","ERROR",obj.errorMensaje);
					}else{
						obj = $.parseJSON($.jCryption.decrypt(data, password_));
						if(obj.idError!==0){
							muestraDialogo("#adicionales2","ERROR",obj.errorMensaje);
							document.frmAdicionales1.email.value="";
						}
					}
			  }).fail(function(jqXHR, textStatus, errorThrown){
				  			muestraDialogo("#adicionales2","ERROR",getMsg(jqXHR,errorThrown));
				        });
	}
}


function getMsg(jqXHR, exception){
	var msg;
	if (jqXHR.status === 0)msg='Not connect.\n Verify Network.';
	else if (jqXHR.status == 404)msg='Requested page not found. [404]';
	else if (jqXHR.status == 500)msg='Internal Server Error [500].';
	else if (exception === 'parsererror')msg='Requested JSON parse failed.';
	else if (exception === 'timeout')msg='Time out error.';
	else if (exception === 'abort')msg='Ajax request aborted.';
	else msg='Uncaught Error.\n' + jqXHR.responseText;
	return msg;
}


function pagar3dSecure(){
	var form = $( "#frmPago" );
	if(!form.valid()){return false;}
	var postData={data:$.jCryption.encrypt("password="+document.frmPago.password.value+"&plataforma="+$("#plataforma").val(), password_)};
	newAjax("infoUsr",postData).done( function( data ) {
				  var obj = null;
				  try{
				  	obj = $.parseJSON(data);
				  	muestraDialogo("#pago","ERROR",obj.errorMensaje);
				  	$("[id=password]").val('');
				  }catch(err){
					//if(typeof obj !== "undefined" && obj !== null && obj.length>0){
					//	muestraDialogo("#pago","ERROR",obj.errorMensaje);
					//}else{
						obj = $.parseJSON($.jCryption.decrypt(data, password_));
						if(obj.idError && obj.idError!==0){
							muestraDialogo("#pago","ERROR",obj.errorMensaje);
						}else{
							document.datos3dSecure.total.value=obj.varTotal;
							document.datos3dSecure.currency.value=obj.varCurrency;
							document.datos3dSecure.address.value=obj.varAddress;
							document.datos3dSecure.order_id.value=obj.varOrderId;
							document.datos3dSecure.merchant.value=obj.varMerchant;
							document.datos3dSecure.store.value=obj.varStore;
							document.datos3dSecure.term.value=obj.varTerm;
							document.datos3dSecure.digest.value=obj.digest;
							document.datos3dSecure.return_target.value="";
							document.datos3dSecure.usuario.value=obj.user;
							document.datos3dSecure.urlBack.value=obj.urlBack;
							document.datos3dSecure.referencia.value=obj.referencia;
							$.post(urlB+"pagina-prosa",$( "#datos3dSecure" ).serialize()).done(function(data) {
								  	$('#procom3DSecureContent').html(data); 
								  	$('#procom3DSecureContent').find('form').attr('data-ajax','false');
								  	$("select[name=_cc_expmonth]").attr("data-role", "none");
								  	$("select[name=_cc_expyear]").attr("data-role", "none");
								 	$("select[name=cc_type]").attr("data-role", "none");
								  	$("select[name=cc_type],select[name=_cc_expmonth],select[name=_cc_expyear]").scroller({preset: 'select', theme: 'android-ics light', mode: 'mixed', lang:'es', display: 'bottom', animate: 'slidedown'});
								  	$("#procom3DSecure").trigger("create"); 
								  	$("input[name=cc_name]").val(obj.nombre+" "+obj.apellido+" "+obj.materno);
									$("input[name=cc_number]").val(obj.tarjeta);
									if(obj.tipotarjeta===1){
										$("select[name=cc_type]").scroller('setValue', 'Visa', true);
									}else if(obj.tipotarjeta===2){
										$("select[name=cc_type]").scroller('setValue', 'Mastercard', true);
									}
									var vigencia=obj.vigencia;
									$("select[name=_cc_expyear]").scroller('setValue', vigencia.substring(3), true);
									$("select[name=_cc_expmonth]").scroller('setValue', vigencia.substring(0,2), true);
									$.mobile.changePage('#procom3DSecure');
								}).fail(function(jqXHR, textStatus, errorThrown) 
								        {
						  			muestraDialogo("#pago","ERROR",getMsg(jqXHR,errorThrown));
						        });
						}
					}
			  }
	).fail(function(jqXHR, textStatus, errorThrown) 
				        {
				  			muestraDialogo("#pago","ERROR",getMsg(jqXHR,errorThrown));
				        }
	);
	
}
function newAjax(url,parm) {
	   return jQuery.ajax({
			  type: "POST",
			  url: urlB+url,
			  data:parm,
			  timeout: 8000,
			  beforeSend: function(){bloqueaUi("Cargando");},
			  complete: function() { $.unblockUI(); }
	   });
}
